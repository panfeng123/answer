
$('.handleCancel').click(function(){
    $('.release').css('display','none')
        editrs.txt.clear()
        $('#formId')[0].reset();
        $('#formId1')[0].reset();
        $('#formId2')[0].reset();
        $('#formId3')[0].reset();
        $('#formId4')[0].reset();
  })
        var channelId=50

        const Ed = window.wangEditor
        const editrs = new Ed('#edit1')
        editrs.config.onchange = function (html) {
            // 第二步，监控变化，同步更新到 textarea
            var re = /<img[^>]+>/g;
        }
        editrs.config.compatibleMode = function () {
            // 返回 true 表示使用兼容模式；返回 false 使用标准模式
            return true
        }
        // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
        editrs.config.onchangeTimeout = 500 // 修改为 500 ms
        // 还可以修改历史记录的步数。默认 30 步
        editrs.config.historyMaxSize = 50 // 修改为 50 步
        editrs.config.linkImgCallback = function (src) {  //插入网络图片的回调
        }

        // 图片上传
        editrs.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
        editrs.config.customUploadImg = function (resultFiles, insertImgFn) {
            let formData= new FormData()
            let files = resultFiles[0]
            formData.append('file', files, files.name)
            $.ajax({
                method:"post",
                headers: {
                    'token': tokens,
                },
                url:"https://api.guandaoxiufu.com/common/upload",
                data: formData,
                dataType: "json",
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (res) {
                    insertImgFn(res.data.fullurl)
                },
                error:function(err){
                }
            })
            // insertImgFn(imgUrl)
        }
        editrs.config.showFullScreen = false //隐藏编辑器全屏按钮
        editrs.config.uploadImgMaxLength = 1 //限制上传图片最多一张
        editrs.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
        editrs.config.showLinkImgHref = false// 隐藏网络图片的href
        editrs.config.pasteFilterStyle = false
        editrs.config.emotions = [
            {
                title: 'QQ', // tab 的标题
                type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
                content:emjoy
            },
        ]
        editrs.config.menus = [
            'head',
            'bold',
            'fontSize',
            'fontName',
            'italic',
            'underline',
            'strikeThrough',
            'indent',
            'foreColor',
            'backColor',
            'list',
            'justify',
            'quote',
            'emoticon',
            'image',
            'splitLine',
            'undo',
        ]
        // emjoy
        editrs.create()


    // 第一个form
    var imgUrl=''
    layui.use(['form', 'layedit', 'laydate','upload'], function(){
        var form = layui.form
        ,layer = layui.layer
        var $ = layui.jquery
          ,upload = layui.upload
      upload.render({
  	  	 elem: '#test1'
  	  	 ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
	  	 ,auto:false
  	  	 ,choose: function(obj){
	 	  obj.preview(function(index, file, result){
	 	  var formData= new FormData()
          formData.append('file', file, file.name)
	 	  $.ajax({
     	    type: "post",
     	    url: "https://api.guandaoxiufu.com/common/upload",
	 		data:formData,
	 		contentType:false,
     	    dataType: "json",
	 		processData: false,
     	    success: function (res) {
	 			// $('.uploadImg').attr('src',res.data.fullurl)
        //         $('.layui-hide').css('cssText','display:block!important')
        //         $('.layui-upload-drag p').css('display','none')
                layui.$('#uploadtest1').removeClass('layui-hide').find('img').attr('src',res.data.fullurl);
                $('#uploadtest1').siblings().hide()
                imgUrl=res.data.fullurl
     	    }
     	 });	
     	});
	 	
  	  	 }
  	  });  
      //自定义验证规则
      form.verify({
        title: function(value){
          if(value.length < 1){
            return '标题不能为空';
          }
        }
        ,contacts: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
          ,contact_number: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
      });
      
      //监听提交
      form.on('submit(form1)', function(data){
        // if(!imgUrl){
        //     layer.msg('请上传图片',{
        //         icon : 5,
        //         shift : 6, 
        //     });
        //     return false
        // }else{
        //     data.field.image=imgUrl
        // }
        if(!editrs.txt.html()){
            layer.msg('请输入正文',{
                icon : 5,
                shift : 6, 
            });
            return false
        }else{
            data.field.content=editrs.txt.html()
        }
        data.field.channel_id=channelId
        console.log(data.field);
        articlePush(data.field).then(res=>{
            if(res.code==1){
                imgUrl=''
                layer.msg(res.msg)
                // $('.uploadImg').attr('src','')
                // $('.layui-hide').css('cssText','display:none!important')
                // $('.layui-upload-drag p').css('display','block')
                layui.$('#uploadtest1').addClass('layui-hide').find('img').attr('src', '');
                $('#uploadtest1').siblings().show()
                $('.release').css('display','none')
                editrs.txt.clear()
                var currentId=''
                if(getUrlParam('pkey')!==null){
                    $('.request_menu ul li[id='+pkey+']').addClass("active")
                    currentId=pkey
                }else{
                    $('.request_menu ul li').eq(0).addClass("active")
                    currentId=$('.request_menu ul li').eq(0).attr('id')
                }
                //根据menu的id获取列表数据
                isSearch=getUrlParam('isSearch'),keys=getParamFromUrl('keys'),channel=getUrlParam('channel')
                if(isSearch){
                    currentId=channel
                }
                getTable(currentId,0,10).then(res=>{
                    menuClick($('.request_menu ul li').eq(0).attr('id'),res)
                }).catch(err=>{

                })
            }else{
                layer.msg(res.msg,{icon : 5,shift : 6});
            }
        })
        
        return false;
      });
    });

    const Ed1 = window.wangEditor
    const editrs1 = new Ed1('#edit2')
    editrs1.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
        var re = /<img[^>]+>/g;
    }
    editrs1.config.compatibleMode = function () {
        // 返回 true 表示使用兼容模式；返回 false 使用标准模式
        return true
    }
    // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
    editrs1.config.onchangeTimeout = 500 // 修改为 500 ms
    // 还可以修改历史记录的步数。默认 30 步
    editrs1.config.historyMaxSize = 50 // 修改为 50 步
    editrs1.config.linkImgCallback = function (src) {  //插入网络图片的回调
    }

    // 图片上传
    editrs1.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
    editrs1.config.customUploadImg = function (resultFiles, insertImgFn) {
        let formData= new FormData()
        let files = resultFiles[0]
        formData.append('file', files, files.name)
        $.ajax({
            method:"post",
            headers: {
                'token': tokens,
            },
            url:"https://api.guandaoxiufu.com/common/upload",
            data: formData,
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                insertImgFn(res.data.fullurl)
            },
            error:function(err){
            }
        })
        // insertImgFn(imgUrl)
    }
    editrs1.config.showFullScreen = false //隐藏编辑器全屏按钮
    editrs1.config.uploadImgMaxLength = 1 //限制上传图片最多一张
    editrs1.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
    editrs1.config.showLinkImgHref = false// 隐藏网络图片的href
    editrs1.config.pasteFilterStyle = false
    editrs1.config.emotions = [
        {
            title: 'QQ', // tab 的标题
            type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
            content:emjoy
        },
    ]
    editrs1.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'indent',
        'foreColor',
        'backColor',
        'list',
        'justify',
        'quote',
        'emoticon',
        'image',
        'splitLine',
        'undo',
    ]
    // emjoy
    editrs1.create()
    // 第一步，初始化 textarea 的值
layui.use(['layer', 'jquery', 'form'], function() {
    var layer = layui.layer,
        $ = layui.jquery,
        form = layui.form;
        var arrs=[]
        if(userinfo){
          if(userinfo.type===0){
            arrs=[{name:"前沿技术",id:51},{name:"找良才",id:55}]
            channelId=51
            $('.proForm4').show().siblings('form').hide()
          }else{
            arrs=[{name:"找伯乐",id:52}]
            channelId=52
            $('.proForm2').show().siblings('form').hide()
          }
          $.each(arrs, function (index, item) {
            $('select[name=channel_id]').append(new Option(item.name, item.id));// 下拉菜单里添加元素
            layui.form.render("select");
        });
        }
        
        
        form.on('select(channel_id)', function(data) {
          channelId=data.value
          imgUrl=''
          layui.$('#uploadtest1').addClass('layui-hide').find('img').attr('src', '');
          $('#uploadtest1').siblings().show()
          layui.$('#uploadtest2').addClass('layui-hide').find('img').attr('src', '');
          $('#uploadtest2').siblings().show()
          layui.$('#uploadtest3').addClass('layui-hide').find('img').attr('src', '');
          $('#uploadtest3').siblings().show()
            if(data.value=='50'){
                $('.proForm1').show().siblings('form').hide()
            }else if(data.value=='51'){
              $('.proForm4').show().siblings('form').hide()
            }else if(data.value=='52'){
                $('.proForm2').show().siblings('form').hide()
            }else if(data.value=='55'){
                $('.proForm3').show().siblings('form').hide()
            }
        })
})
  
    // 第二个form
      //配置插件目录
    layui.config({
      base: './mods/'
      , version: '1.0'
    });
    var imgUrl=''
    layui.use(['form', 'layedit', 'laydate','upload', 'layarea'], function(){
      var form = layui.form
      ,layer = layui.layer
      var $ = layui.jquery
      ,upload = layui.upload
      , layarea = layui.layarea    
        layarea.render({
            elem: '#area-form2',
            data: {
              province: '--选择省--',
              city: '--选择市--',
              county: '--选择区--',
            },
            change: function (res) {
                //选择结果
                // console.log(res);
            }
        });
      upload.render({
  	  	 elem: '#test2'
  	  	 ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
	  	 ,auto:false
  	  	 ,choose: function(obj){
	 	  obj.preview(function(index, file, result){
	 	  var formData= new FormData()
          formData.append('file', file, file.name)
	 	  $.ajax({
     	    type: "post",
     	    url: "https://api.guandaoxiufu.com/common/upload",
	 		data:formData,
	 		contentType:false,
     	    dataType: "json",
	 		processData: false,
     	    success: function (res) {
	 			// $('.uploadImg').attr('src',res.data.fullurl)
        //         $('.layui-hide').css('cssText','display:block!important')
        //         $('.layui-upload-drag p').css('display','none')
                layui.$('#uploadtest2').removeClass('layui-hide').find('img').attr('src',res.data.fullurl);
                $('#uploadtest2').siblings().hide()
                imgUrl=res.data.fullurl
     	    }
     	 });	
     	});
	 	
  	  	 }
  	  });  
      //自定义验证规则
      form.verify({
        title: function(value){
          if(value.length < 1){
            return '标题不能为空';
          }
        }
        ,age: function(value){
            if(!value){
              return '年龄不能为空';
            }
          }
          ,education: function(value){
            if(!value){
              return '学历不能为空';
            }
          }
          ,school: function(value){
            if(!value){
              return '学校不能为空';
            }
          }
          ,work_experience: function(value){
            if(!value){
              return '工作经历不能为空';
            }
          }
          ,good_at: function(value){
            if(!value){
              return '个人擅长不能为空';
            }
          }
          ,expected_work: function(value){
            if(!value){
              return '期望工作不能为空';
            }
          }
          
        ,salary_expectation: function(value){
          if(!value){
            return '期望薪资不能为空';
          }
        }
        ,contacts: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
          ,contact_number: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
      });
      
      //监听提交
      form.on('submit(form2)', function(data){
        // if(!imgUrl){
        //     layer.msg('请上传图片',{
        //         icon : 5,
        //         shift : 6, 
        //     });
        //     return false
        // }else{
        //     data.field.image=imgUrl
        // }
        if(!editrs1.txt.html()){
            layer.msg('请输入正文',{
                icon : 5,
                shift : 6, 
            });
            return false
        }else{
            data.field.content=editrs1.txt.html()
        }
        data.field.channel_id=channelId
        articlePush(data.field).then(res=>{
            if(res.code==1){
                imgUrl=''
                layer.msg(res.msg)
                // $('.uploadImg').attr('src','')
                // $('.layui-hide').css('cssText','display:none!important')
                // $('.layui-upload-drag p').css('display','block')
                layui.$('#uploadtest2').addClass('layui-hide').find('img').attr('src','');
                $('#uploadtest2').siblings().show()
                $('.release').css('display','none')
                editrs1.txt.clear()
                var currentId=''
                if(getUrlParam('pkey')!==null){
                    $('.request_menu ul li[id='+pkey+']').addClass("active")
                    currentId=pkey
                }else{
                    $('.request_menu ul li').eq(0).addClass("active")
                    currentId=$('.request_menu ul li').eq(0).attr('id')
                }
                //根据menu的id获取列表数据
                isSearch=getUrlParam('isSearch'),keys=getParamFromUrl('keys'),channel=getUrlParam('channel')
                if(isSearch){
                    currentId=channel
                }
                getTable(currentId,0,10).then(res=>{
                    menuClick($('.request_menu ul li').eq(0).attr('id'),res)
                }).catch(err=>{

                })
            }else{
                layer.msg(res.msg,{icon : 5,shift : 6});
            }
        })
        
        return false;
      });
    });




    const Ed2 = window.wangEditor
    const editrs2 = new Ed2('#edit3')
    editrs2.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
        var re = /<img[^>]+>/g;
    }
    editrs2.config.compatibleMode = function () {
        // 返回 true 表示使用兼容模式；返回 false 使用标准模式
        return true
    }
    // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
    editrs2.config.onchangeTimeout = 500 // 修改为 500 ms
    // 还可以修改历史记录的步数。默认 30 步
    editrs2.config.historyMaxSize = 50 // 修改为 50 步
    editrs2.config.linkImgCallback = function (src) {  //插入网络图片的回调
    }

    // 图片上传
    editrs2.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
    editrs2.config.customUploadImg = function (resultFiles, insertImgFn) {
        let formData= new FormData()
        let files = resultFiles[0]
        formData.append('file', files, files.name)
        $.ajax({
            method:"post",
            headers: {
                'token': tokens,
            },
            url:"https://api.guandaoxiufu.com/common/upload",
            data: formData,
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                insertImgFn(res.data.fullurl)
            },
            error:function(err){
            }
        })
        // insertImgFn(imgUrl)
    }
    editrs2.config.showFullScreen = false //隐藏编辑器全屏按钮
    editrs2.config.uploadImgMaxLength = 1 //限制上传图片最多一张
    editrs2.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
    editrs2.config.showLinkImgHref = false// 隐藏网络图片的href
    editrs2.config.pasteFilterStyle = false
    editrs2.config.emotions = [
        {
            title: 'QQ', // tab 的标题
            type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
            content:emjoy
        },
    ]
    editrs2.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'indent',
        'foreColor',
        'backColor',
        'list',
        'justify',
        'quote',
        'emoticon',
        'image',
        'splitLine',
        'undo',
    ]
    // emjoy
    editrs2.create()
    // 第三个form
    var imgUrl=''
    layui.use(['form', 'layedit', 'laydate','upload'], function(){
      var form = layui.form
      ,layer = layui.layer
      var $ = layui.jquery
      ,upload = layui.upload
      upload.render({
  	  	 elem: '#test3'
  	  	 ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
	  	 ,auto:false
  	  	 ,choose: function(obj){
	 	  obj.preview(function(index, file, result){
	 	  var formData= new FormData()
          formData.append('file', file, file.name)
	 	  $.ajax({
     	    type: "post",
     	    url: "https://api.guandaoxiufu.com/common/upload",
	 		data:formData,
	 		contentType:false,
     	    dataType: "json",
	 		processData: false,
     	    success: function (res) {
	 			// $('.uploadImg').attr('src',res.data.fullurl)
        //         $('.layui-hide').css('cssText','display:block!important')
        //         $('.layui-upload-drag p').css('display','none')
        layui.$('#uploadtest3').removeClass('layui-hide').find('img').attr('src',res.data.fullurl);
                $('#uploadtest3').siblings().hide()
                imgUrl=res.data.fullurl
     	    }
     	 });	
     	});
	 	
  	  	 }
  	  });  
      //自定义验证规则
      form.verify({
        title: function(value){
          if(value.length < 1){
            return '标题不能为空';
          }
        }
        ,position: function(value){
            if(!value){
              return '岗位不能为空';
            }
          }
          ,salary_range: function(value){
            if(!value){
              return '薪酬范围不能为空';
            }
          }
          ,work_area: function(value){
            if(!value){
              return '工作区域不能为空';
            }
          }
          ,job_descriptio: function(value){
            if(!value){
              return '岗位描述不能为空';
            }
          }
          ,age: function(value){
            if(!value){
              return '年龄不能为空';
            }
          }
          ,education: function(value){
            if(!value){
              return '学历不能为空';
            }
          }
          ,working_years: function(value){
            if(!value){
              return '工作年限不能为空';
            }
          }
        ,contacts: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
          ,contact_number: function(value){
            if(!value){
              return '联系人不能为空';
            }
          }
      });
      
      //监听提交
      form.on('submit(form3)', function(data){
        // if(!imgUrl){
        //     layer.msg('请上传图片',{
        //         icon : 5,
        //         shift : 6, 
        //     });
        //     return false
        // }else{
        //     data.field.image=imgUrl
        // }
        if(!editrs2.txt.html()){
            layer.msg('请输入正文',{
                icon : 5,
                shift : 6, 
            });
            return false
        }else{
            data.field.content=editrs2.txt.html()
        }
        data.field.channel_id=channelId
        console.log(data.field);
        articlePush(data.field).then(res=>{
            if(res.code==1){
                imgUrl=''
                layer.msg(res.msg)
                // $('.uploadImg').attr('src','')
                // $('.layui-hide').css('cssText','display:none!important')
                // $('.layui-upload-drag p').css('display','block')
                layui.$('#uploadtest3').addClass('layui-hide').find('img').attr('src','');
                $('#uploadtest3').siblings().show()
                $('.release').css('display','none')
                editrs2.txt.clear()
                var currentId=''
                if(getUrlParam('pkey')!==null){
                    $('.request_menu ul li[id='+pkey+']').addClass("active")
                    currentId=pkey
                }else{
                    $('.request_menu ul li').eq(0).addClass("active")
                    currentId=$('.request_menu ul li').eq(0).attr('id')
                }
                //根据menu的id获取列表数据
                isSearch=getUrlParam('isSearch'),keys=getParamFromUrl('keys'),channel=getUrlParam('channel')
                if(isSearch){
                    currentId=channel
                }
                getTable(currentId,0,10).then(res=>{
                    menuClick($('.request_menu ul li').eq(0).attr('id'),res)
                }).catch(err=>{

                })
            }else{
                layer.msg(res.msg,{icon : 5,shift : 6});
            }
        })
        
        return false;
      });
    });


//前沿技术富文本
    const Ed4 = window.wangEditor
    const editrs4 = new Ed4('#edit4')
    editrs4.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
        var re = /<img[^>]+>/g;
    }
    editrs4.config.compatibleMode = function () {
        // 返回 true 表示使用兼容模式；返回 false 使用标准模式
        return true
    }
    // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
    editrs4.config.onchangeTimeout = 500 // 修改为 500 ms
    // 还可以修改历史记录的步数。默认 30 步
    editrs4.config.historyMaxSize = 50 // 修改为 50 步
    editrs4.config.linkImgCallback = function (src) {  //插入网络图片的回调
    }

    // 图片上传
    editrs4.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
    editrs4.config.customUploadImg = function (resultFiles, insertImgFn) {
        let formData= new FormData()
        let files = resultFiles[0]
        formData.append('file', files, files.name)
        $.ajax({
            method:"post",
            headers: {
                'token': tokens,
            },
            url:"https://api.guandaoxiufu.com/common/upload",
            data: formData,
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                insertImgFn(res.data.fullurl)
            },
            error:function(err){
            }
        })
        // insertImgFn(imgUrl)
    }
    editrs4.config.showFullScreen = false //隐藏编辑器全屏按钮
    editrs4.config.uploadImgMaxLength = 1 //限制上传图片最多一张
    editrs4.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
    editrs4.config.showLinkImgHref = false// 隐藏网络图片的href
    editrs4.config.pasteFilterStyle = false
    editrs4.config.emotions = [
        {
            title: 'QQ', // tab 的标题
            type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
            content:emjoy
        },
    ]
    editrs4.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'indent',
        'foreColor',
        'backColor',
        'list',
        'justify',
        'quote',
        'emoticon',
        'image',
        'splitLine',
        'undo',
    ]
    // emjoy
    editrs4.create()
    layui.use(['form', 'layedit', 'laydate','upload'], function(){
      var form = layui.form
      ,layer = layui.layer
      var $ = layui.jquery
        ,upload = layui.upload
    upload.render({
       elem: '#test4'
       ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
     ,auto:false
       ,choose: function(obj){
     obj.preview(function(index, file, result){
     var formData= new FormData()
        formData.append('file', file, file.name)
     $.ajax({
         type: "post",
         url: "https://api.guandaoxiufu.com/common/upload",
     data:formData,
     contentType:false,
         dataType: "json",
     processData: false,
         success: function (res) {
       // $('.uploadImg').attr('src',res.data.fullurl)
      //         $('.layui-hide').css('cssText','display:block!important')
      //         $('.layui-upload-drag p').css('display','none')
              layui.$('#uploadtest1').removeClass('layui-hide').find('img').attr('src',res.data.fullurl);
              $('#uploadtest1').siblings().hide()
              imgUrl=res.data.fullurl
         }
      });	
     });
   
       }
    });  
    //自定义验证规则
    form.verify({
      title: function(value){
        if(value.length < 1){
          return '标题不能为空';
        }
      }
      // ,contacts: function(value){
      //     if(!value){
      //       return '联系人不能为空';
      //     }
      //   }
      //   ,contact_number: function(value){
      //     if(!value){
      //       return '联系人不能为空';
      //     }
      //   }
    });
    
    //监听提交
    form.on('submit(form4)', function(data){
      // if(!imgUrl){
      //     layer.msg('请上传图片',{
      //         icon : 5,
      //         shift : 6, 
      //     });
      //     return false
      // }else{
      //     data.field.image=imgUrl
      // }
      if(!editrs4.txt.html()){
          layer.msg('请输入正文',{
              icon : 5,
              shift : 6, 
          });
          return false
      }else{
          data.field.content=editrs4.txt.html()
      }
      data.field.channel_id=channelId
      articlePush(data.field).then(res=>{
          if(res.code==1){
              imgUrl=''
              layer.msg(res.msg)
              // $('.uploadImg').attr('src','')
              // $('.layui-hide').css('cssText','display:none!important')
              // $('.layui-upload-drag p').css('display','block')
              layui.$('#uploadtest1').addClass('layui-hide').find('img').attr('src', '');
              $('#uploadtest1').siblings().show()
              $('.release').css('display','none')
              editrs4.txt.clear()
              var currentId=''
              if(getUrlParam('pkey')!==null){
                  $('.request_menu ul li[id='+pkey+']').addClass("active")
                  currentId=pkey
              }else{
                  $('.request_menu ul li').eq(0).addClass("active")
                  currentId=$('.request_menu ul li').eq(0).attr('id')
              }
              //根据menu的id获取列表数据
              isSearch=getUrlParam('isSearch'),keys=getParamFromUrl('keys'),channel=getUrlParam('channel')
              if(isSearch){
                  currentId=channel
              }
              getTable(currentId,0,10).then(res=>{
                  menuClick($('.request_menu ul li').eq(0).attr('id'),res)
              }).catch(err=>{

              })
          }else{
              layer.msg(res.msg,{icon : 5,shift : 6});
          }
      })
      
      return false;
    });
  });