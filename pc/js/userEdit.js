// wangEditFn()
//资料案例

Array.prototype.remove = function(val) { 
  var index = this.indexOf(val); 
  if (index > -1) { 
  this.splice(index, 1); 
  } 
  };
window.wordLeg = function (obj) {
  var currleg = $(obj).val().length;
  var length = $(obj).attr('maxlength');
  if (currleg > length) {
    layer.msg('字数请在' + length + '字以内');
  } else {
    $('.text_count').text(currleg);
  }
}


var case_doc = ''
var case_doc_name = ''
var image = ''
layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
  var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload;
  $('select#pushSelect').html('')
  var pushSelectArr = []
  if (JSON.parse(localStorage.getItem("userInfo")).type === 0) {
    pushSelectArr = [{ name: "有问必答", id: "26" },
    { name: "项目牵线", id: "27" },
    { name: "资料案例", id: "29" },
    { name: "人物技术", id: "30" },
    { name: "新闻交流", id: "31" },
    { name: "配套商家", id: "28" }]
  } else {
    pushSelectArr = [{ name: "回答", id: "26" },
    { name: "项目", id: "27" },
    { name: "案例", id: "29" },
    { name: "技术", id: "30" }]
  }
  $.each(pushSelectArr, function (index, item) {
    $('select#pushSelect').append(new Option(item.name, item.id));// 下拉菜单里添加元素
    layui.form.render("select");
  });
  $('select#technologyChannel').html('')
  if (userinfo) {
    if (userinfo.type === 0) {
      arrs = [{ name: "找良才", id: 55 },{ name: "前沿技术", id: 51 }]
    } else {
      arrs = [{ name: "找伯乐", id: 52 }]
    }
    $.each(arrs, function (index, item) {
      $('select#technologyChannel').append(new Option(item.name, item.id));// 下拉菜单里添加元素
      layui.form.render("select");
    });
  }

  // upload.render({
  //   elem: '#test8'
  //   ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
  //   ,auto: false
  //   ,multiple: true
  //   ,accept: 'file'
  //   ,choose: function(obj){
  //       obj.preview(function(index, file, result){
  //         var formData= new FormData()
  //         formData.append('file', file, file.name)
  //         $.ajax({
  //           type: "post",
  //           url: "https://api.guandaoxiufu.com/common/upload",
  //           data:formData,
  //           contentType:false,
  //           dataType: "json",
  //           processData: false,
  //           success: function (res) {
  //             if(res.code==1){
  //               layer.msg('上传成功')
  //               case_doc=res.data.fullurl
  //               $('.showfileName').html(file.name)
  //               $('.showfileName').show()
  //            }else{
  //               layer.msg(res.msg,{icon:5,shift:6})
  //            }
  //           }
  //         });	
  //    });
  //  }
  // });
})

function formSelect(parentId, channelId, isEidt, detailInfo) {
  $('#materialId').hide()
  $('#newExId').hide()
  $('#answerId').hide()
  $('#projectIds').hide()
  $('#project1Id').hide()
  $('#project2Id').hide()
  $('#project3Id').hide()
  $('#technologyIds').hide()
  $('#technology1Id').hide()
  $('#technology2Id').hide()
  $('#technology3Id').hide()
  $('#merchantsFormId').hide()
  if (parentId == 26) {
    var image = ''
    $('#answerId').show()
    layui.$('#answerUploadImg').addClass('layui-hide').find('img').attr('src', '');
    $('#answerUploadImg').siblings().show()
    if (isEidt) {
      editor2.txt.html(detailInfo.data.archivesInfo.content)
    }
    layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
      var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload;
      upload.render({
        elem: '#answerUpload'
        , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
        , auto: false
        , choose: function (obj) {
          obj.preview(function (index, file, result) {
            var formData = new FormData()
            formData.append('file', file, file.name)
            uploadImgFn(formData).then(res => {
              layui.$('#answerUploadImg').removeClass('layui-hide').find('img').attr('src', res);
              $('#answerUploadImg').siblings().hide()
              image = res
            })
          });
        }
      });
      if (isEidt) {
        form.val('answerId', {
          "channel_id": detailInfo.data.archivesInfo.channel_id// "name": "value"
          , "title": detailInfo.data.archivesInfo.title
        }
        );
        if (detailInfo.data.archivesInfo.image) {
          layui.$('#answerUploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
          $('#answerUploadImg').siblings().hide()
          imgUrl = detailInfo.data.archivesInfo.image
        }
      }
      //自定义验证规则
      form.verify({
        title: function (value) {
          if (value.length < 1) {
            return '标题不能为空';
          }
        }
      });

      //监听提交
      form.on('submit(answerForm)', function (data) {
        // if(!image){
        //     layer.msg('请上传图片',{
        //         icon : 5,
        //         shift : 6, 
        //     });
        //     return false
        // }else{
        //     data.field.image=image
        // }
        if (!editor2.txt.html()) {
          layer.msg('请输入正文', {
            icon: 5,
            shift: 6,
          });
          return false
        } else {
          data.field.content = editor2.txt.html()
        }
        if (isEidt) {
          data.field.id = detailInfo.data.archivesInfo.id;
        }
        data.field.channel_id=channelId
        if (isEidt) {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articleEdit(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layer.msg(res.msg);
              layui.$('#answerUploadImg').addClass('layui-hide').find('img').attr('src', ' ');
              $('#answerUploadImg').siblings().show()
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor2.txt.clear()
              $('#answerId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        } else {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articlePush(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layer.msg(res.msg);
              layui.$('#answerUploadImg').addClass('layui-hide').find('img').attr('src', ' ');
              $('#answerUploadImg').siblings().show()
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor2.txt.clear()
              $('#answerId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        }
        return false;
      });
    });
  } else if (parentId == 27) {
    $('#projectIds').show()
    layui.use(['laypage', 'layer'], function () {
      var form = layui.form
      if (isEidt) {
        form.val('projectIds', {
          "channel_id": detailInfo.data.archivesInfo.channel_id// "name": "value"
        }
        );
      }
      
    })
    if (channelId == 36) {
      var imgUrl = ''
      $('#project1Id').show()
      layui.$('#project1UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#project1UploadImg').siblings().show()
      if (isEidt) {
        editor3.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.config({
        base: './mods/'
        , version: '1.0'
      });
      layui.use(['form', 'layedit', 'laydate', 'upload', 'layarea'], function () {
        var form = layui.form,
          layer = layui.layer,
          $ = layui.jquery,
          upload = layui.upload,
          layarea = layui.layarea
        if (isEidt) {
          form.val('project1Id', {
            "category": detailInfo.data.archivesInfo.category,
            "title": detailInfo.data.archivesInfo.title,
            "province": detailInfo.data.archivesInfo.area
            , "requirement": detailInfo.data.archivesInfo.requirement
            , "project_scale": detailInfo.data.archivesInfo.project_scale
            , "partners": detailInfo.data.archivesInfo.partners
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
          );
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#project1UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#project1UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
          // detailInfo.data.archivesInfo.area='湖北省'
          //  layarea.render({
          //     elem: '#area-push',
          //     data: {
          //       province: '--选择省--',
          //   },
          //     change: function (res) {

          //     }
          // });
        } else {
          //   form.val('project1Id', {
          //     "category": detailInfo.data.archivesInfo.category,
          //     "title": detailInfo.data.archivesInfo.title,
          //     "province":420100
          //     ,"requirement": detailInfo.data.archivesInfo.requirement
          //     ,"project_scale": detailInfo.data.archivesInfo.project_scale
          //     ,"partners": detailInfo.data.archivesInfo.partners
          //     ,"contacts": detailInfo.data.archivesInfo.contacts
          //     ,"contact_number": detailInfo.data.archivesInfo.contact_number
          //     }
          // );
        }
        upload.render({
          elem: '#project1Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#project1UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#project1UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , requirement: function (value) {
            if (!value) {
              return '技术要求不能为空';
            }
          }
          , project_scale: function (value) {
            if (!value) {
              return '项目规模不能为空';
            }
          }
          , partners: function (value) {
            if (!value) {
              return '对合作方需求不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系电话不能为空';
            }
          }
        });
        // form.on('select(category)', function(data){
        //           console.log(data);
        //       })
        //监听提交
        form.on('submit(project1Form)', function (data) {
          // if(!imgUrl){
          //     layer.msg('请上传图片',{
          //         icon : 5,
          //         shift : 6, 
          //     });
          //     return false
          // }else{
          //     data.field.imgUrl=imgUrl
          // }
          if (!editor3.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor3.txt.html()
          }
          if (data.field.province == '') {
            layer.msg('请选择地点', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.area = data.field.province
          }
          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project1UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project1UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor3.txt.clear()
                $('#project1Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project1UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project1UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor3.txt.clear()
                $('#project1Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }
          return false;
        });
      });
    } else if (channelId == 37) {
      var imgUrl = ''
      $('#project2Id').show()
      layui.$('#project2UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#project2UploadImg').siblings().show()
      if (isEidt) {
        editor4.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.use(['form', 'layedit', 'laydate', 'upload', 'layarea'], function () {
        var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload, layarea = layui.layarea
        if(isEidt){
          form.val('project2Id', {
            "category": detailInfo.data.archivesInfo.category,
            "title": detailInfo.data.archivesInfo.title,
            "province": detailInfo.data.archivesInfo.area
            , "good_craft": detailInfo.data.archivesInfo.good_craft
            , "biggest_feature": detailInfo.data.archivesInfo.biggest_feature
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
         );
        }
        
        if (isEidt) {
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#project2UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#project2UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
        }
        upload.render({
          elem: '#project2Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#project2UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#project2UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , good_craft: function (value) {
            if (!value) {
              return '擅长工艺不能为空';
            }
          }
          , biggest_feature: function (value) {
            if (!value) {
              return '最大特点不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系电话不能为空';
            }
          }
        });

        //监听提交
        form.on('submit(project2Form)', function (data) {
          // if(!imgUrl){
          //     layer.msg('请上传图片',{
          //         icon : 5,
          //         shift : 6, 
          //     });
          //     return false
          // }else{
          //     data.field.imgUrl=imgUrl
          // }
          if (!editor4.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor4.txt.html()
          }
          if (data.field.province == '') {
            layer.msg('请选择地点', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.area = data.field.province
          }
          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project2UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project2UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor4.txt.clear()
                $('#project2Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project2UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project2UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor4.txt.clear()
                $('#project2Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }

          return false;
        });
      });
    } else if (channelId == 38 || channelId == 56) {
      var imgUrl = ''
      $('#project3Id').show()
      layui.$('#project3UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#project3UploadImg').siblings().show()
      if (isEidt) {
        editor5.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.use(['form', 'layedit', 'laydate', 'upload', 'layarea'], function () {
        var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload, layarea = layui.layarea
        if (isEidt) {
          form.val('project3Id', {
            "category": detailInfo.data.archivesInfo.category,
            "title": detailInfo.data.archivesInfo.title,
            "province": detailInfo.data.archivesInfo.area
            , "good_craft": detailInfo.data.archivesInfo.good_craft
            , "biggest_feature": detailInfo.data.archivesInfo.biggest_feature
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
          );
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#project3UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#project3UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
        }
        upload.render({
          elem: '#project3Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#project3UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#project3UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系电话不能为空';
            }
          }
        });

        //监听提交
        form.on('submit(project3Form)', function (data) {
          // if(!imgUrl){
          //     layer.msg('请上传图片',{
          //         icon : 5,
          //         shift : 6, 
          //     });
          //     return false
          // }else{
          //     data.field.imgUrl=imgUrl
          // }
          if (!editor5.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor5.txt.html()
          }
          if (data.field.province == '') {
            layer.msg('请选择地点', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.area = data.field.province
          }
          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project3UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project3UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor5.txt.clear()
                $('#project3Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg);
                layui.$('#project3UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#project3UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor5.txt.clear()
                $('#project3Id')[0].reset();
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }

          return false;
        });
      });
    }
  } else if (parentId == 28) {
    $('#merchantsFormId').show()
    var image = ''
    layui.$('#merchantsFormBox').addClass('layui-hide').find('img').attr('src', '');
    $('#merchantsFormBox').siblings().show()
    if (isEidt) {
      editor9.txt.html(detailInfo.data.archivesInfo.content)
    }
    
    layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
      var form = layui.form, layer = layui.layer, layedit = layui.layedit, $ = layui.jquery, upload = layui.upload;
      if (isEidt) {
        form.val('merchantsFormId', {
          "description": detailInfo.data.archivesInfo.description,
          "category": detailInfo.data.archivesInfo.category,
          "title": detailInfo.data.archivesInfo.title,
          "channel_id": detailInfo.data.archivesInfo.channel_id
        }
        );
        if (detailInfo.data.archivesInfo.image) {
          layui.$('#merchantsFormBox').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
          $('#merchantsFormBox').siblings().hide()
          image = detailInfo.data.archivesInfo.image
        }
      }
      upload.render({
        elem: '#merchantsUpload'
        , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
        , auto: false
        , choose: function (obj) {
          obj.preview(function (index, file, result) {
            var formData = new FormData()
            formData.append('file', file, file.name)
            uploadImgFn(formData).then(res => {
              layui.$('#merchantsFormBox').removeClass('layui-hide').find('img').attr('src', res);
              $('#merchantsFormBox').siblings().hide()
              image = res
            })
          });
    
        }
      });
      form.verify({
        title: function (value) {
          if (value.length < 1) {
            return '标题不能为空';
          }
        }
        , content: function (value) {
          layedit.sync(editIndex);
        },
        description: function (value) {
          if (!value) {
            return '简介不能为空';
          }
        }
      });
      form.on('submit(merchantsForm)', function (data) {
        if (!image) {
          layer.msg('请上传图片', {
            icon: 5,
            shift: 6,
          });
          return false
        } else {
          data.field.image = image
        }
        if (!editor9.txt.html()) {
          layer.msg('请输入正文', {
            icon: 5,
            shift: 6,
          });
          return false
        } else {
          data.field.content = editor9.txt.html()
        }
        if (isEidt) {
          data.field.id = detailInfo.data.archivesInfo.id;
        }
        if (isEidt) {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articleEdit(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layer.msg(res.msg);
              layui.$('#merchantsFormBox').addClass('layui-hide').find('img').attr('src', ' ');
              $('#merchantsFormBox').siblings().show()
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor9.txt.clear()
              $('#merchantsFormId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        } else {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articlePush(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layer.msg(res.msg);
              layui.$('#merchantsFormBox').addClass('layui-hide').find('img').attr('src', ' ');
              $('#merchantsFormBox').siblings().show()
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor9.txt.clear()
              $('#merchantsFormId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        }

        return false;
      });
    })
  } else if (parentId == 29) {
    $('#materialId').show()
    var imgUrl = ''
    layui.$('#materialUploadImg').addClass('layui-hide').find('img').attr('src', '');
    $('#materialUploadImg').siblings().show()
    if (isEidt) {
      editor.txt.html(detailInfo.data.archivesInfo.content)
    }
    var imgList = []
    var fileListName=[]
    layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
      var form = layui.form, layer = layui.layer, layedit = layui.layedit, $ = layui.jquery, upload = layui.upload;
      //自定义验证规则
      upload.render({
        elem: '#materialUpload'
        , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
        , auto: false
        , choose: function (obj) {
          obj.preview(function (index, file, result) {
            var formData = new FormData()
            formData.append('file', file, file.name)
            uploadImgFn(formData).then(res => {
              layui.$('#materialUploadImg').removeClass('layui-hide').find('img').attr('src', res);
              $('#materialUploadImg').siblings().hide()
              imgUrl = res
            })
          });
        }
      });
      if (isEidt) {
        var fujianName = detailInfo.data.archivesInfo.case_doc_name.split(',').filter(item => item != '')
        var fujianUrl = detailInfo.data.archivesInfo.case_doc.split(',').filter(item => item != '')
        var lengthList = fujianName.length
        var oldLst = ''
        for (let i = 0; i < lengthList; i++) {
          oldLst += `<tr id="upload-'+ ${i} +'">'
            <td> ${fujianName[i]}</td>
            <td><span  class="dddd" names="${fujianName[i]}" url="${fujianUrl[i]}" style="background-color: #FF5722;height: 22px;
            line-height: 22px;
            padding: 0 5px;
            font-size: 12px;color: #fff;display: inline-block;cursor: pointer;">删除</span></td>
          </tr>`
        }
        $('#demoList').append(oldLst)
        $('.dddd').click(function(){
          let index=$(this).attr('url')
          let names=$(this).attr('names')
          $(this).parent().parent().remove()
          fujianUrl.remove(index)
          fujianName.remove(names)
          case_doc=fujianUrl.join(',')
          case_doc_name=fujianName.join(',')
        })
      }
      var uploadListIns = upload.render({
        elem: '#test8'
        , elemList: $('#demoList') //列表元素对象
        , url: 'https://api.guandaoxiufu.com/common/upload'
        , accept: 'file'
        , multiple: true
        , number: 3
        , auto: true
        , choose: function (obj) {
          var that = this;
          var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
          //读取本地文件
          obj.preview(function (index, file, result) {
            imgList=[]
            fileListName=[]
            var tr = $(['<tr id="upload-' + index + '">'
              , '<td>' + file.name + '</td>'
              , '<td>'
              , '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
              , '</td>'
              , '</tr>'].join(''));

            //删除
            tr.find('.demo-delete').on('click', function () {
              delete files[index]; //删除对应的文件
              tr.remove();
              uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
              imgList.splice(index, 1)
              fileListName.splice(index, 1)
              case_doc=imgList.join(',')
              case_doc_name=case_doc_name.join(',')
            });
            that.elemList.append(tr);
          });
        }
        , done: function (res, index, upload) { //成功的回调
          var that = this;
          //if(res.code == 0){ //上传成功
         
          var tr = that.elemList.find('tr#upload-' + index)
            , tds = tr.children();
            case_doc=case_doc==""?(case_doc+res.data.fullurl):(case_doc+','+res.data.fullurl)
            case_doc_name=case_doc_name==""?(case_doc_name+res.data.filename):(case_doc_name+','+res.data.filename)
            imgList.push(res.data.fullurl)
            fileListName.push(res.data.filename)
          // tds.eq(3).html(''); //清空操作
          delete this.files[index]; //删除文件队列已经上传成功的文件
          return;
          //}
          this.error(index, upload);
        }
        , allDone: function (obj) { //多文件上传完毕后的状态回调
          console.log(obj)
        }
        , error: function (index, upload) { //错误回调
          var that = this;
          var tr = that.elemList.find('tr#upload-' + index)
            , tds = tr.children();
          tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
        }
      });
      var fileNames = ''
      if (isEidt) {
        if (detailInfo.data.archivesInfo.case_doc) {
          let fileList = detailInfo.data.archivesInfo.case_doc.split('/').filter(item=>item!="")
          fileNames = fileList[fileList.length - 1]
        }
        case_doc = detailInfo.data.archivesInfo.case_doc ? detailInfo.data.archivesInfo.case_doc : ''
        case_doc_name = detailInfo.data.archivesInfo.case_doc_name ? detailInfo.data.archivesInfo.case_doc_name : ''
      }
      $('.showfileName').html(fileNames)
      if (isEidt) {
        detailInfo.data.archivesInfo.case_doc ? $('.showfileName').show() : $('.showfileName').hide()
        form.val('materialId', {
          "channel_id": detailInfo.data.archivesInfo.channel_id// "name": "value"
          , "category": detailInfo.data.archivesInfo.category
          , "title": detailInfo.data.archivesInfo.title
          , "case_doc": detailInfo.data.archivesInfo.case_doc ? detailInfo.data.archivesInfo.case_doc : ''
        }
        );
        if (detailInfo.data.archivesInfo.image) {
          layui.$('#materialUploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
          $('#materialUploadImg').siblings().hide()
          imgUrl = detailInfo.data.archivesInfo.image
        }
      }
      form.verify({
        title: function (value) {
          if (value.length < 1) {
            return '标题不能为空';
          }
        }
        , content: function (value) {
          layedit.sync(editIndex);
        }
      });

      //监听提交
      form.on('submit(materialForm)', function (data) {
        // if(!imgUrl){
        //     layer.msg('请上传图片',{
        //         icon : 5,
        //         shift : 6, 
        //     });
        //     return false
        // }else{
        //     data.field.image=imgUrl
        // }

        // if(!case_doc){
        //   layer.msg('请上传附件',{
        //       icon : 5,
        //       shift : 6, 
        //   });
        //   return false
        // }else{
        // data.field.case_doc=case_doc
        // }
        if (!editor.txt.html()) {
          layer.msg('请输入正文', {
            icon: 5,
            shift: 6,
          });
          return false
        } else {
          data.field.content = editor.txt.html()
        }
        if (isEidt) {
          data.field.id = detailInfo.data.archivesInfo.id;
        }
        data.field.case_doc = case_doc
        data.field.case_doc_name = case_doc_name
        if (isEidt) {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articleEdit(data.field).then(res => {
            if (res.code == 1) {
              imgUrl = ''
              case_doc = ''
              layer.msg(res.msg);
              layui.$('#materialUploadImg').addClass('layui-hide').find('img').attr('src', ' ');
              $('#materialUploadImg').siblings().show()
              $('#demoList').html('')
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor.txt.clear()
              $('#materialId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        } else {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articlePush(data.field).then(res => {
            if (res.code == 1) {
              imgUrl = ''
              case_doc = ''
              layer.msg(res.msg);
              layui.$('#materialUploadImg').addClass('layui-hide').find('img').attr('src', ' ');
              $('#materialUploadImg').siblings().show()
              layer.closeAll('loading');
              $('#demoList').html('')
              $('.release').css('display', 'none')
              editor.txt.clear()
              $('#materialId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        }
        return false;
      });
    });
  } else if (parentId == 30) {
    $('#technologyIds').show()
    var arrs = []
    
    layui.use(['laypage', 'layer'], function () {
      var form = layui.form
      if (isEidt) {
        form.val('technologyIds', {
          "channel_id": detailInfo.data.archivesInfo.channel_id// "name": "value"
        }
        );
      }
      // form.on('select(channel_id)', function (data) {
      //   imgUrl = ''
      //   layui.$('#technology1UploadImg').addClass('layui-hide').find('img').attr('src', '');
      //   $('#technology1UploadImg').siblings().show()
      //   layui.$('#technology2UploadImg').addClass('layui-hide').find('img').attr('src', '');
      //   $('#technology2UploadImg').siblings().show()
      //   layui.$('#technology3UploadImg').addClass('layui-hide').find('img').attr('src', '');
      //   $('#technology3UploadImg').siblings().show()
      //   if (data.value == '50' || data.value == '51') {
      //     $('#technology1Id').show()
      //     $('#technology2Id').hide()
      //     $('#technology3Id').hide()
      //   } else if (data.value == '52') {
      //     $('#technology1Id').hide()
      //     $('#technology2Id').show()
      //     $('#technology3Id').hide()
      //   } else if (data.value == '55') {
      //     $('#technology1Id').hide()
      //     $('#technology2Id').hide()
      //     $('#technology3Id').show()
      //   }
      // })
    })
    if (channelId == 50 || channelId == 51) {
      var imgUrl = ''
      $('#technology1Id').show()
      layui.$('#technology1UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#technology1UploadImg').siblings().show()
      if (isEidt) {
        editor6.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
        var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload
        upload.render({
          elem: '#technology1Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#technology1UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#technology1UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        if (isEidt) {
          form.val('technology1Id', {
            "title": detailInfo.data.archivesInfo.title
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
          );
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#technology1UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#technology1UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
        }
        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
        });

        //监听提交
        form.on('submit(technologyForm1)', function (data) {
          // if (!imgUrl) {
          //   layer.msg('请上传图片', {
          //     icon: 5,
          //     shift: 6,
          //   });
          //   return false
          // } else {
          //   data.field.image = imgUrl
          // }
          if (!editor6.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor6.txt.html()
          }
          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology1UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology1UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor6.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology1UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology1UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor6.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }

          return false;
        });
      });
    } else if (channelId == 52) {
      var imgUrl = ''
      $('#technology2Id').show()
      layui.$('#technology2UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#technology2UploadImg').siblings().show()
      if (isEidt) {
        editor7.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.use(['form', 'layedit', 'laydate', 'upload', 'layarea'], function () {
        var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload, layarea = layui.layarea
        if (isEidt) {
          // detailInfo.data.archivesInfo.area='湖北省'
          // layarea.render({
          //     elem: '#area-technology',
          //     data: {
          //       province: '',
          //     },
          //     change: function (res) {
          //     }
          // });
        }
        upload.render({
          elem: '#technology2Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#technology2UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#technology2UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        if (isEidt) {
          form.val('technology2Id', {
            "title": detailInfo.data.archivesInfo.title
            , "age": detailInfo.data.archivesInfo.age
            , "education": detailInfo.data.archivesInfo.education
            , "school": detailInfo.data.archivesInfo.school
            , "work_experience": detailInfo.data.archivesInfo.work_experience
            , "good_at": detailInfo.data.archivesInfo.good_at
            , "expected_work": detailInfo.data.archivesInfo.expected_work
            , "salary_expectation": detailInfo.data.archivesInfo.salary_expectation
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
          );
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#technology2UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#technology2UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
        }

        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , age: function (value) {
            if (!value) {
              return '年龄不能为空';
            }
          }
          , education: function (value) {
            if (!value) {
              return '学历不能为空';
            }
          }
          , school: function (value) {
            if (!value) {
              return '学校不能为空';
            }
          }
          , work_experience: function (value) {
            if (!value) {
              return '工作经历不能为空';
            }
          }
          , good_at: function (value) {
            if (!value) {
              return '个人擅长不能为空';
            }
          }
          , expected_work: function (value) {
            if (!value) {
              return '期望工作不能为空';
            }
          }

          , salary_expectation: function (value) {
            if (!value) {
              return '期望薪资不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
        });

        //监听提交
        form.on('submit(technologyForm2)', function (data) {
          // if (!imgUrl) {
          //   layer.msg('请上传图片', {
          //     icon: 5,
          //     shift: 6,
          //   });
          //   return false
          // } else {
          //   data.field.image = imgUrl
          // }
          if (!editor7.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor7.txt.html()
          }

          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology2UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology2UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology2UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology2UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }

          return false;
        });
      });
    } else {
      var imgUrl = ''
      $('#technology3Id').show()
      layui.$('#technology3UploadImg').addClass('layui-hide').find('img').attr('src', '');
      $('#technology3UploadImg').siblings().show()
      if (isEidt) {
        editor8.txt.html(detailInfo.data.archivesInfo.content)
      }
      layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
        var form = layui.form, layer = layui.layer, $ = layui.jquery, upload = layui.upload
        upload.render({
          elem: '#technology3Upload'
          , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
          , auto: false
          , choose: function (obj) {
            obj.preview(function (index, file, result) {
              var formData = new FormData()
              formData.append('file', file, file.name)
              uploadImgFn(formData).then(res => {
                layui.$('#technology3UploadImg').removeClass('layui-hide').find('img').attr('src', res);
                $('#technology3UploadImg').siblings().hide()
                imgUrl = res
              })
            });
          }
        });
        if (isEidt) {
          form.val('technology3Id', {
            "title": detailInfo.data.archivesInfo.title
            , "position": detailInfo.data.archivesInfo.position
            , "salary_range": detailInfo.data.archivesInfo.salary_range
            , "work_area": detailInfo.data.archivesInfo.work_area
            , "job_descriptio": detailInfo.data.archivesInfo.job_descriptio
            , "age": detailInfo.data.archivesInfo.age
            , "education": detailInfo.data.archivesInfo.education
            , "working_years": detailInfo.data.archivesInfo.working_years
            , "contacts": detailInfo.data.archivesInfo.contacts
            , "contact_number": detailInfo.data.archivesInfo.contact_number
          }
          );
          if (detailInfo.data.archivesInfo.image) {
            layui.$('#technology3UploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
            $('#technology3UploadImg').siblings().hide()
            imgUrl = detailInfo.data.archivesInfo.image
          }
        }
        //自定义验证规则
        form.verify({
          title: function (value) {
            if (value.length < 1) {
              return '标题不能为空';
            }
          }
          , position: function (value) {
            if (!value) {
              return '岗位不能为空';
            }
          }
          , salary_range: function (value) {
            if (!value) {
              return '薪酬范围不能为空';
            }
          }
          , work_area: function (value) {
            if (!value) {
              return '工作区域不能为空';
            }
          }
          , job_descriptio: function (value) {
            if (!value) {
              return '岗位描述不能为空';
            }
          }
          , age: function (value) {
            if (!value) {
              return '年龄不能为空';
            }
          }
          , education: function (value) {
            if (!value) {
              return '学历不能为空';
            }
          }
          , working_years: function (value) {
            if (!value) {
              return '工作年限不能为空';
            }
          }
          , contacts: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
          , contact_number: function (value) {
            if (!value) {
              return '联系人不能为空';
            }
          }
        });

        //监听提交
        form.on('submit(technologyForm3)', function (data) {
          // if (!imgUrl) {
          //   layer.msg('请上传图片', {
          //     icon: 5,
          //     shift: 6,
          //   });
          //   return false
          // } else {
          //   data.field.image = imgUrl
          // }
          if (!editor8.txt.html()) {
            layer.msg('请输入正文', {
              icon: 5,
              shift: 6,
            });
            return false
          } else {
            data.field.content = editor8.txt.html()
          }

          if (isEidt) {
            data.field.id = detailInfo.data.archivesInfo.id;
          }
          data.field.channel_id=channelId
          if (isEidt) {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articleEdit(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology3UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology3UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor8.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          } else {
            layer.msg('正在发布', {
              icon: 16,
              time: 10000
            });
            articlePush(data.field).then(res => {
              if (res.code == 1) {
                imgUrl = ''
                layer.msg(res.msg)
                layui.$('#technology3UploadImg').addClass('layui-hide').find('img').attr('src', '');
                $('#technology3UploadImg').siblings().show()
                layer.closeAll('loading');
              $('.release').css('display', 'none')
                editor8.txt.clear()
                if (userInfo.type === 0) {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                    showPage(res.data.page.count)
                  })
                } else {
                  getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                    menuClick($('.request_menu>ul li.active').attr("id"), res)
                    showPage(res.data.page.count)
                  }).catch(err => { })
                }
              } else {
                layer.msg(res.msg, { icon: 5, shift: 6 });
              }
            })
          }

          return false;
        });
      });
    }
  } else if (parentId == 31) {
    $('#newExId').show()
    var image = ''
    layui.$('#newUploadImg').addClass('layui-hide').find('img').attr('src', '');
    $('#newUploadImg').siblings().show()
    if (isEidt) {
      editor1.txt.html(detailInfo.data.archivesInfo.content)
    }
    layui.use(['form', 'layedit', 'laydate', 'upload'], function () {
      var form = layui.form, layer = layui.layer, layedit = layui.layedit, $ = layui.jquery, upload = layui.upload;
      upload.render({
        elem: '#newUpload'
        , url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
        , auto: false
        , choose: function (obj) {
          obj.preview(function (index, file, result) {
            var formData = new FormData()
            formData.append('file', file, file.name)
            uploadImgFn(formData).then(res => {
              layui.$('#newUploadImg').removeClass('layui-hide').find('img').attr('src', res);
              $('#newUploadImg').siblings().hide()
              imgUrl = res
            })
          });
        }
      });

      //自定义验证规则
      if (isEidt) {
        form.val('newExId', {
          "channel_id": detailInfo.data.archivesInfo.channel_id// "name": "value"
          , "title": detailInfo.data.archivesInfo.title
        });
        if (detailInfo.data.archivesInfo.image) {
          layui.$('#newUploadImg').removeClass('layui-hide').find('img').attr('src', detailInfo.data.archivesInfo.image);
          $('#newUploadImg').siblings().hide()
          image = detailInfo.data.archivesInfo.image
        }
      }
      form.verify({
        title: function (value) {
          if (value.length < 1) {
            return '标题不能为空';
          }
        }
        , content: function (value) {
          layedit.sync(editIndex);
        }
      });

      //监听提交
      form.on('submit(newExForm)', function (data) {
        // if (!image) {
        //   layer.msg('请上传图片', {
        //     icon: 5,
        //     shift: 6,
        //   });
        //   return false
        // } else {
        //   data.field.image = image
        // }
        if (!editor1.txt.html()) {
          layer.msg('请输入正文', {
            icon: 5,
            shift: 6,
          });
          return false
        } else {
          data.field.content = editor1.txt.html()
        }
        if (isEidt) {
          data.field.id = detailInfo.data.archivesInfo.id;
        }
        if (isEidt) {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articleEdit(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layui.$('#newUploadImg').addClass('layui-hide').find('img').attr('src', '');
              $('#newUploadImg').siblings().show()
              layer.msg(res.msg);
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor1.txt.clear()
              $('#newExId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        } else {
          layer.msg('正在发布', {
            icon: 16,
            time: 10000
          });
          articlePush(data.field).then(res => {
            if (res.code == 1) {
              image = ''
              layui.$('#newUploadImg').addClass('layui-hide').find('img').attr('src', '');
              $('#newUploadImg').siblings().show()
              layer.msg(res.msg);
              layer.closeAll('loading');
              $('.release').css('display', 'none')
              editor1.txt.clear()
              $('#newExId')[0].reset();
              if (userInfo.type === 0) {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, $('.request_menu>ul li.active').attr('ids'), $('.request_menu .proSmall li.active').attr('id')).then(res => {
                  showPage(res.data.page.count)
                })
              } else {
                getTable(ids, $('.request_menu>ul li.active').find('a').html(), 0, false).then(res => {
                  menuClick($('.request_menu>ul li.active').attr("id"), res)
                  showPage(res.data.page.count)
                }).catch(err => { })
              }
            } else {
              layer.msg(res.msg, { icon: 5, shift: 6 });
            }
          })
        }
        return false;
      });
    });
  }

}