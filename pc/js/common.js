$(function(){
	
	//登录输入框效果
	$('.form_text_ipt input').focus(function(){
		$(this).parent().css({
			'box-shadow':'0 0 3px #bbb',
		});
	});
	$('.form_text_ipt input').blur(function(){
		$(this).parent().css({
			'box-shadow':'none',
		});
		//$(this).parent().next().hide();
	});
	
	//表单验证
	$('.form_text_ipt input').bind('input propertychange',function(){
		if($(this).val()==""){
			$(this).css({
				'color':'red',
			});
			$(this).parent().css({
				'border':'solid 1px red',
			});
			//$(this).parent().next().find('span').html('helow');
			$(this).parent().next().show();
		}else{
			$(this).css({
				'color':'#ccc',
			});
			$(this).parent().css({
				'border':'solid 1px #ccc',
			});
			$(this).parent().next().hide();
		}
	});
});

//时间转换
function formatDate(dates,type) {
	var date = new Date(dates*1000);
	var YY = date.getFullYear() 
	// var YY = date.getFullYear() + '-';
	var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) ;
	var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
	var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	if(type==1){
		return YY + '年'
	}else if(type==2){
		return YY + '年' + MM  +'月'+ DD +'日'+" "+hh + mm + ss;
	}
  }

//超过一万保留两位小数  
function unitZan(num){
	var n
	if(num===undefined||num===null){
		return 0
	}
	if(num>10000){
	  n = Math.floor((num /10000) * 100) / 100;
	  return n + "万";
	}else if(num==0){
	  return ''
	}else{
		return num
	}
}


//等级图标
function levelImg(level){
	var levelHtml=''
	if(level){
        var guang=parseInt(level/64)
		var sunNum=parseInt(level%64/16)
		var moonNum=parseInt(level%64%16/4)
		var starNum=parseInt(level%4)
        for(let i=0;i<guang;i++){
			levelHtml=levelHtml+'<img style="width:15px;height:15px;transform:scale(1.4);" src="./images/huang_2.png" alt="">'
		}
		for(let i=0;i<sunNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_sun.png" alt="">'
		}
		for(let i=0;i<moonNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_moon.png" alt="">'
		}
		for(let i=0;i<starNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_star.png" alt="">'
		}
	}
	return levelHtml
}

  //获取参数
  function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

//获取中文参数
function getParamFromUrl(name){
    var r = getParamString(name)
    return r
  }
    //获取参数
function getParamString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return decodeURI(r[2]); return null; //返回参数值
}

function wangEditFn(eleName){
	const EdEditor = window.wangEditor
    const hasEditrs = new EdEditor(eleName)
    hasEditrs.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
        var re = /<img[^>]+>/g;
    }
    hasEditrs.config.compatibleMode = function () {
        // 返回 true 表示使用兼容模式；返回 false 使用标准模式
        return true
    }
    // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
    hasEditrs.config.onchangeTimeout = 500 // 修改为 500 ms
    // 还可以修改历史记录的步数。默认 30 步
    hasEditrs.config.historyMaxSize = 50 // 修改为 50 步
    hasEditrs.config.linkImgCallback = function (src) {  //插入网络图片的回调
    }

    // 图片上传
    hasEditrs.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
    hasEditrs.config.customUploadImg = function (resultFiles, insertImgFn) {
        let formData= new FormData()
        let files = resultFiles[0]
        formData.append('file', files, files.name)
        $.ajax({
            method:"post",
            headers: {
                'token': tokens,
            },
            url:"https://api.guandaoxiufu.com/common/upload",
            data: formData,
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                insertImgFn(res.data.fullurl)
            },
            error:function(err){
            }
        })
        // insertImgFn(imgUrl)
    }
    hasEditrs.config.showFullScreen = false //隐藏编辑器全屏按钮
    hasEditrs.config.uploadImgMaxLength = 1 //限制上传图片最多一张
    hasEditrs.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
    hasEditrs.config.showLinkImgHref = false// 隐藏网络图片的href
    hasEditrs.config.pasteFilterStyle = false
    hasEditrs.config.emotions = [
        {
            title: 'QQ', // tab 的标题
            type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
            content:emjoy
        },
    ]
    hasEditrs.config.menus = [
        'head',
        'bold',
        'fontSize',
        'fontName',
        'italic',
        'underline',
        'strikeThrough',
        'indent',
        'foreColor',
        'backColor',
        'list',
        'justify',
        'quote',
        'emoticon',
        'image',
        'splitLine',
        'undo',
    ]
    // emjoy
    hasEditrs.create()

	return hasEditrs
    // 第一步，初始化 textarea 的值
}


function uploadRender(eleName,imgBox,upload,layui){
        upload.render({
            elem: eleName
            ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
            ,auto:false
            ,choose: function(obj){
                obj.preview(function(index, file, result){
                    var formData= new FormData()
                    formData.append('file', file, file.name)
                    
                });
            }
        }); 
}