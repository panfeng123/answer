var tokens=localStorage.getItem("token")?JSON.parse(localStorage.getItem("token")):''
var userinfo=JSON.parse(localStorage.getItem("userInfo"));
var hrefUrl=window.location.href.replace(/www.guandaoxiufu.com/, "m.guandaoxiufu.com")
var formDateType=1
console.log(hrefUrl);
// $(window).resize(function(){
  if($(window).width()<=640){
    window.location.href=hrefUrl
  }
// });
layui.use('element', function(){
  var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
  //监听导航点击
  element.on('nav(demo)', function(elem){
    //console.log(elem)
    layer.msg(elem.text());
  });
});
if(getUrlParam('pkey')!==null){
  $('.request_menu ul li[id='+getUrlParam('pkey')+']').addClass("active").siblings().removeClass('active')
}else{
  $('.request_menu ul li').eq(0).addClass("active").siblings().removeClass('active')
}
teamworkApi().then(res=>{
  var str='https://api.guandaoxiufu.com'
  if(res.code==1){
    $('.callName').html(res.data[3].values)
    $('.callPhone').html(res.data[2].values)
    $('.callAddress').html(res.data[1].values)
    $('.callEmail').html(res.data[4].values)
    if(res.data[0].values[0].indexOf(str)==-1){
      res.data[0].values[0]=str+res.data[0].values[0]
    }
    $('.teamworkCode img').attr('src',res.data[0].values[0])
  }
})
//点击合作投稿
$('.handleTeam .together').click(function(){
	$('.teamworks').css('display','flex')
})

//关闭合作投稿
$('.teamworkHeader .icon-close').click(function(){
	$('.teamworks').css('display','none')
})
var mianUserUrl=''
if(userinfo){
  if(userinfo.type===0){
    mianUserUrl='company.html' 
  }else{
    mianUserUrl='userInfo.html'
  }
}

if(!tokens){
    $('.search_btn').removeClass('haslogin')
    var str=`<a href="login.html">登录</a>
    <i></i>
    <a href="register.html">注册</a>`
}else{  
    $('.search_btn').addClass('haslogin')
    // userInfo.html?id=${userinfo.id}
    var str=`<div class="userInfo">
                <img class="userImages" src="${userinfo.avatar?userinfo.avatar:''}" alt="">
                <span class="userNickName" style="font-size:18px;">${userinfo.type===0?userinfo.company_name_str:userinfo.nickname}</span>
                <div class="userHandle">
                     <div>
                         <span></span>
                         <div>
                             <p><a class="jumpToUser" href="${mianUserUrl}?id=${userinfo.id}&pkey=28">我的主页</a></p>
                             <p><a class="editInfo" href="javascript:;">修改资料</a></p>
                             <p><a class="userExit" href="javascript:;">退出</a></p>
                         </div>
                     </div>
                 </div>
            </div>`
}
$(str).appendTo($('.search_btn'))
var canReq=false
headerImg().then(res=>{
    $('.header_login>img').attr('src',res.data.sitelogo)
    $('.header_login>span').attr('src',res.data.sitename)
    $('.qrCode img').eq(0).attr('src',res.data.qrcode[0])
    $('.qrCode img').eq(1).attr('src',res.data.qrcode[1])
    if(res.code==1){
      formDateType=res.data.timeformat=='date'?1:2
      canReq=true
      if(window.location.href.indexOf('index.html')!==-1||window.location.href.indexOf('.html')===-1){
        var keywords=$("meta[name=keywords]")[0];
        keywords.content=res.data.keywords
        var description=$("meta[name=description]")[0];
        description.content=res.data.description
        $('title').html(res.data.title+'-管道修复网')
      }else if(window.location.href.indexOf('company.html')!==-1||window.location.href.indexOf('userInfo.html')!==-1){
        var keywords=$("meta[name=keywords]")[0];
        keywords.content=res.data.keywords
        var description=$("meta[name=description]")[0];
        description.content=res.data.description
        
      }
    }
})
$(document).click(function(e){
    var e = e || window.event; //浏览器兼容性
    var elem = e.target || e.srcElement;
    if(elem.className=='userImages'||elem.className=='userNickName'){
      $('.userHandle').css('display')=='none'?$('.userHandle').css('display','block'):$('.userHandle').css('display','none')
    }else if(elem.className=='userExit'){
          localStorage.clear()
          location.reload()
          // window.location.href='login.html'
    }else{
      $('.userHandle').css('display','none')
    }
})

$('.editInfo').on('click',function(){
  editInfo().then(res=>{
    let {type}=res.data;
    if(type==0){
       window.location.href='companyinfo.html'
    }
    if(type==1){
      window.location.href='usercenter.html'
    }
  })
})


    
layui.use(['form'], function(){
  var form = layui.form
  ,layer = layui.layer
  form.on('radio(ChoiceRadio)', function(data){
  });    
});  
var bigSearchType=''
var demo2 = xmSelect.render({
  el: '#demo2', 
  radio: true,
  clickClose: true,
  theme: {
    color: '#ff9900',
    },
    repeat: true,
            direction: 'down',
  data: [
    {name: '有问必答', value: 26, href:'answerMust.html' ,selected: true,},
    {name: '项目牵线', value: 27, href:'project.html'},
    {name: '配套商家', value: 28, href:'merchants.html'},
    {name: '资料案例', value: 29, href:'material.html'},
    {name: '人物技术', value: 30, href:'technology.html'},
    {name: '新闻交流', value: 31, href:'newExchange.html'},
  ],
  on: function(data){
    bigSearchType=data
  },
})
$('.search_input .icon-search').click(function(){
    if($(this).prev().val()===''){
      if(bigSearchType){
        window.location.href=bigSearchType.change[0].href
      }else{
        window.location.href='answerMust.html'
      }
    }else{
      if(bigSearchType){
        window.location.href=bigSearchType.change[0].href+'?isSearch=search&keys='+$(this).prev().val()+'&channel='+bigSearchType.change[0].value
      }else{
        window.location.href='answerMust.html?isSearch=search&keys='+$(this).prev().val()+'&channel=26'
      }
    }
})

$('.pageCollect').click(function(){
  _addFavorite()
})
function _addFavorite() {
  var url = window.location;
  var title = document.title;
  var ua = navigator.userAgent.toLowerCase();
  if (ua.indexOf("360se") > -1) {
      alert("由于360浏览器功能限制，请按 Ctrl+D 手动收藏！");
  }
  else if (ua.indexOf("msie 8") > -1) {
      window.external.AddToFavoritesBar(url, title); //IE8
  }
  else if (document.all) {//IE类浏览器
    try{
     window.external.addFavorite(url, title);
    }catch(e){
     alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
    }
  }
  else if (window.sidebar) {//firfox等浏览器；
      window.sidebar.addPanel(title, url, "");
  }
  else {
      alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
  }
}  
