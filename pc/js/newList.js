function newList(questList,ids){
    var str=''
    for(let i=0;i<questList.length;i++){
        if(questList[i].channel.parent_id==26){
            questList[i].showComments=false
            var isZan=unitZan(questList[i].likes)
            if(questList[i].comment.user_info){
                var accountLevel=levelImg(questList[i].comment.user_info.level)
            }else{
                accountLevel=''
            }
            var userLevel=levelImg(questList[i].user_info.level)
            str=str+`<li class="answerMust">
                                <div class="questitle">
                                    <div class="userQuestion clearfix " >
                                         <div class="QuestionTit jumpTodetail" urls="answerBody.html" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                            【${questList[i].channel.name}】 ${questList[i].title}
                                         </div>
                                         <div class="userInfo">
                                            <a target='${questList[i].user_id===0?'':'_blank'}' class='${questList[i].user_id===0?'cancelPointer':''}' href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                                                <span>${questList[i].user_info.nickname}</span>
                                                `+userLevel+`    
                                            </a>
                                         </div>
                                    </div>
                                    <div class="QuestionDetail clearfix jumpTodetail" urls="answerBody.html"  ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                         <img src="${questList[i].comment.images}" class="${questList[i].comment.images?'':'hide'}">   
                                         <div class="threeHide ${questList[i].comment.images?'newDesc':''}">
                                             ${questList[i].comment.content?questList[i].comment.content:''}
                                         </div>
                                    </div>
                                </div>
                               <div class="timeAndZan clearfix">
                                    <div class="zanLeft">
                                        <a href="javascript:;" class="favor ${questList[i].is_praise==1?'upFavor':''} ${tokens?'':'unTaken'}" index=${i} itemId=${questList[i].id}>
                                            <i class="icon iconfont icon-up"></i>
                                            <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                                        </a>
                                        <a href="javascript:;" class="cancelfavor" index=${i}  itemId=${questList[i].id}>
                                            <i class="icon iconfont icon-down"></i>
                                        </a>
                                        <a href="javascript:;" class="commentIcon ${questList[i].is_praise==2?'downFavor':''}" index=${i} ids=${questList[i].id}>
                                            <i class="icon iconfont icon-message showComment"></i>
                                            <span class="showComment">${questList[i].comments} 评论</span>
                                        </a>    
                                    </div>
                                    <div class="zanRight">
                                        <a target='_blank' href="${questList[i].comment.user_info?(questList[i].comment.user_info.id!=1?(questList[i].comment.user_info.type==0?`company.html?id=${questList[i].comment.user_info.id}`:`userInfo.html?id=${questList[i].comment.user_info.id}`):'javascript:;'):'javascript:;'}">
                                            <span>${questList[i].comment.user_info?(questList[i].comment.user_info.nickname?questList[i].comment.user_info.nickname:''):''}</span>
                                            `+accountLevel+`
                                        </a>    
                                    </div>
                               </div>
                               <div class="commentContent"></div>
                               <div class="pages"></div>
                           </li>`
        }else if(questList[i].channel.parent_id==27){
            var showTel=''
                    if(questList[i].channel_id=='36'||questList[i].channel_id=='56'){
                        showTel=tokens?(questList[i].is_view==1?(questList[i].filed_news.contact_number?questList[i].filed_news.contact_number:''):'***********'):'***********'
                    }else{
                        showTel=questList[i].filed_news?questList[i].filed_news.contact_number:''
                    }
                    var isZan=unitZan(questList[i].likes)
                    var levelHtml=levelImg(questList[i].user_info.level)
                    var times
                    setTimeout(function(){
                        console.log(formDateType);
                        times=formatDate(questList[i].updatetime,formDateType)
                    }(),0)
                    str=str+`<li  class="project">
                            <div class="questitle " style="display:block;word-wrap: break-word;" >
                                 <div class="userQuestion clearfix">
                                      <div class="QuestionTit singleHide jumpTodetail" urls="biddingInfo.html" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                        【${questList[i].channel.name}】 ${questList[i].title}
                                      </div>
                                      <div>
                                        <a target='${questList[i].user_id===0?'':'_blank'}' class="${questList[i].user_id===0?'cancelPointer':''}  ${questList[i].channel_id=='36'||questList[i].channel_id=='56'?(questList[i].is_view==1?'':'hide'):''}" href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                                            <span class="singleHide ${questList[i].user_id===0?'hide':''}">${questList[i].user_info.company_name_str?questList[i].user_info.company_name_str:questList[i].user_info.nickname}</span>
                                            `+levelHtml+`
                                        </a>
                                      </div>
                                 </div>
                                 <div class="QuestionDetail jumpTodetail" urls="biddingInfo.html" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                      <p class="doubleHide">
                                        ${questList[i].description}
                                      </p>
                                 </div>
                            </div>
                             <p class="infos">
                                 <span class="${questList[i].channel_id=='36'||questList[i].channel_id=='56'?'':'hide'} ${tokens?'hide':''}">【<a href="login.html">登录可见联系方式</a>】</span>
                                 <span class="${questList[i].channel_id=='36'||questList[i].channel_id=='56'?'':'hide'} ${questList[i].is_view=='0'?'':'hide'} ${tokens?'':'hide'}">【<a class="seeCall" index='${i}' ids=${questList[i].id} href="javascript:;">查看联系方式</a>】</span>
                             </p>
                            <div class="timeAndZan clearfix">
                                <a href="javascript:;" class="favor ${questList[i].is_praise==1?'upFavor':''} ${tokens?'':'unTaken'}" index=${i} itemId=${questList[i].id}>
                                    <i class="icon iconfont icon-up"></i>
                                    <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                                </a>
                                <a href="javascript:;" class="cancelfavor ${questList[i].is_praise==2?'downFavor':''}" index=${i}  itemId=${questList[i].id}>
                                    <i class="icon iconfont icon-down"></i>
                                </a>
                                <span>`+times+`</span>
                                <span class="personInfo ${questList[i].channel_id=='38'?'hide':''}">联系人：${questList[i].filed_news?questList[i].filed_news.contacts:''}</span>
                                <span class="callInfo ${questList[i].channel_id=='38'?'hide':''}">联系电话：`+showTel+`</span>
                            </div>
                        </li>`
        }else if(questList[i].channel.parent_id==28){
            var userLevel=levelImg(questList[i].user_info.level)
            str=str+`<li class="answerMust">
                                <div class="questitle">
                                    <div class="userQuestion clearfix " >
                                         <div class="QuestionTit jumpTodetail" urls="merchantDetail.html" ids="${questList[i].user_info.id}" articlId=${questList[i].id} pkey=${questList[i].channel_id}>
                                             ${questList[i].title}
                                         </div>
                                         <div class="userInfo">
                                            <a target='${questList[i].user_id===0?'':'_blank'}' class='${questList[i].user_id===0?'cancelPointer':''}' href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                                                <span>${questList[i].user_info.company_name_str}</span>
                                                `+userLevel+`    
                                            </a>
                                         </div>
                                    </div>
                                    <div class="QuestionDetail clearfix jumpTodetail" urls="merchantDetail.html"  articlId=${questList[i].id} ids=${questList[i].user_info.id} pkey=${questList[i].channel_id}>
                                         <img src="${questList[i].image}" class="${questList[i].image?'':'hide'}">   
                                         <div class="threeHide ${questList[i].image?'newDesc':''}">
                                             ${questList[i].description?questList[i].description:''}
                                         </div>
                                    </div>
                                </div>
                           </li>`
        }else if(questList[i].channel.parent_id==29){
            var showTel=tokens?questList[i].phone:'***********'
                    var isZan=unitZan(questList[i].likes)
                    var times
                    setTimeout(function(){
                        console.log(formDateType);
                        times=formatDate(questList[i].updatetime,formDateType)
                    }(),0)
                    var userLevel=levelImg(questList[i].user_info.level)
                    str=str+`<li  class="material">
                                    <div class="questitle " style="display:block;word-wrap: break-word;" >
                                        <div class="userQuestion clearfix">
                                            <div class="QuestionTit singleHide jumpTodetail" urls="materialDetail.html"  ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                                【${questList[i].channel.name}】${questList[i].title}
                                            </div>
                                            <p class='singb'>
                                            <a target='${questList[i].user_id===0?'':'_blank'}' class="${questList[i].user_id===0?'cancelPointer':''} userInfo singleHide" href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                                                <span class="singleHide">${questList[i].user_info.type===0?questList[i].user_info.company_name_str:questList[i].user_info.nickname} </span>
                                                `+userLevel+`
                                            </a>
                                            </p>
                                        </div>
                                        <div class="QuestionDetail jumpTodetail" urls="materialDetail.html" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                            <p class="doubleHide">
                                                ${questList[i].description}
                                            </p>
                                        </div>
                                    </div>
                                   <div class="timeAndZan clearfix">
                                        <div class="zanLeft">
                                            <a href="javascript:;" class="favor ${questList[i].is_praise==1?'upFavor':''} ${tokens?'':'unTaken'}" index=${i} itemId=${questList[i].id}>
                                                <i class="icon iconfont icon-up"></i>
                                                <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                                            </a>
                                            <a href="javascript:;" class="cancelfavor" index=${i}  itemId=${questList[i].id}>
                                                <i class="icon iconfont icon-down"></i>
                                            </a>
                                            <a href="javascript:;" class="commentIcon ${questList[i].is_praise==2?'downFavor':''}" index=${i} ids=${questList[i].id}>
                                                <i class="icon iconfont icon-message showComment"></i>
                                                <span class="showComment">${questList[i].comments} 评论</span>
                                            </a>
                                            <span>`+times+`</span>
                                        </div>    
                                   </div>
                                   <div class="commentContent"></div>
                                   <div class="pages"></div>
                               </li>`
        }else if(questList[i].channel.parent_id==30){
            var isZan=unitZan(questList[i].likes)
            var times
            setTimeout(function(){
                console.log(formDateType);
                times=formatDate(questList[i].updatetime,formDateType)
            }(),0)
            var showTel=''
            var levelHtml=levelImg(questList[i].user_info.level)
            if(questList[i].channel_id==52){
                showTel=tokens?(questList[i].is_view==1?(questList[i].filed_news.contact_number?questList[i].filed_news.contact_number:''):'***********'):'***********'
            }else{
                showTel=questList[i].filed_news?(questList[i].filed_news.contact_number?questList[i].filed_news.contact_number:''):''
            }
            str=str+`<li class="technology">
                            <div class=" listTitle" style="display:block;word-wrap: break-word;" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                <div class="userQuestion clearfix">
                                    <div class="QuestionTit jumpTodetail singleHide" urls="technologyDetail.html" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                       【${questList[i].channel.name}】${questList[i].title}
                                    </div>
                                    <a target='${questList[i].user_id===0?'':'_blank'}' class="${questList[i].user_id===0?'cancelPointer':''} ${questList[i].channel_id=='52'?(questList[i].is_view==1?'':'hide'):''}" href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                                        <span class="singleHide">${questList[i].user_info.company_name_str?questList[i].user_info.company_name_str:questList[i].user_info.nickname}</span>
                                        `+levelHtml+`
                                    </a>
                                </div>
                                <div class="QuestionDetail">
                                    <p class="doubleHide">
                                        ${questList[i].description}
                                    </p>
                                </div>
                            </div>
                            <p class="infos">
                                <span class=" ${tokens?'hide':''} ${questList[i].channel_id==52?'':'hide'}">【<a target='_blank' href="login.html">登录可见联系方式</a>】</span>
                            <span class="${questList[i].channel_id==52?'':'hide'} ${questList[i].is_view=='0'?'':'hide'} ${tokens?'':'hide'}">【<a class="seeCall" index='${i}' ids=${questList[i].id} href="javascript:;">查看联系方式</a>】</span>
                            </p>
                           <div class="timeAndZan clearfix">
                                <div class="zanLeft">
                                    <a href="javascript:;" class="favor ${questList[i].is_praise==1?'upFavor':''} ${tokens?'':'unTaken'}" index=${i} itemId=${questList[i].id}>
                                        <i class="icon iconfont icon-up"></i>
                                        <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                                     </a>
                                     <a href="javascript:;" class="cancelfavor ${questList[i].is_praise==2?'downFavor':''}" index=${i}  itemId=${questList[i].id}>
                                         <i class="icon iconfont icon-down"></i>
                                     </a>
                                    <span>${times}</span>
                                    <span class="personInfo ${questList[i].channel_id==52?'':'hide'}">联系人：${questList[i].filed_news?questList[i].filed_news.contacts:''}</span>
                                    <span class="callInfo ${questList[i].channel_id==52?'':'hide'}">联系电话：${showTel}</span>
                                </div>    
                           </div>
                       </li>`
        }else if(questList[i].channel.parent_id==31){
            var isZan=unitZan(questList[i].likes)
                    var times
                    setTimeout(function(){
                        console.log(formDateType);
                        times=formatDate(questList[i].updatetime,formDateType)
                    }(),0)
                    str=str+`<li  class="newExchange">
                                    <a class="jumpTodetail" urls="newDetail.html" style="display:block;word-wrap: break-word;" ids=${questList[i].id} pkey=${questList[i].channel_id}>
                                        <div class="userQuestion clearfix">
                                            <div class="QuestionTit singleHide">
                                                【${questList[i].channel.name}】${questList[i].title}
                                            </div>
                                        </div>
                                        <div class="QuestionDetail">
                                            <p class="doubleHide">
                                                ${questList[i].description}
                                            </p>
                                        </div>
                                    </a>
                                   <div class="timeAndZan clearfix">
                                        <div class="zanLeft">
                                            <a href="javascript:;" class="favor ${questList[i].is_praise==1?'upFavor':''} ${tokens?'':'unTaken'}" index=${i} itemId=${questList[i].id}>
                                               <i class="icon iconfont icon-up"></i>
                                               <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                                           </a>
                                           <a href="javascript:;" class="cancelfavor" index=${i}  itemId=${questList[i].id}>
                                               <i class="icon iconfont icon-down"></i>
                                           </a>
                                            <span>${times}</span>
                                        </div>    
                                   </div>
                               </li>`
        }
    }
    return str
}