//有问必答列表下的评论列表
function commentInfo(commemtList,parent,names,isDetail){
    var cstr=''
    parent.find('.commentList').remove()
    for(let i=0;i<commemtList.length;i++){
        var ccstr=''
        var showAll=''
        if(commemtList[i].children_commentList){
            for(let j=0;j<commemtList[i].children_commentList.length;j++){
                showAll=''
                if(commemtList[i].children_commentList.length>2){
                    showAll=`<div class="showAll"><a href="javascript:;" id="${commemtList[i].id}">查看全部 ${commemtList[i].children_commentList.length}条回复</a><div>`
                }
                if(j<2&&j>=0){
                    let fuwenben=commemtList[i].children_commentList[j].content
                    // fuwenben = fuwenben.replace(/(\n)/g, "");    
                    // fuwenben = fuwenben.replace(/(\t)/g, "");    
                    // fuwenben = fuwenben.replace(/(\r)/g, "");    
                    // fuwenben = fuwenben.replace(/<\/?[^>]*>/g, "");    
                    // fuwenben = fuwenben.replace(/\s*/g, "");
                    ccstr=ccstr+`<div class="replayItem">
                                    <div class="userHead clearfix">
                                        <a target="_blank" href="${commemtList[i].children_commentList[j].user.type==0?`company.html?pkey=28&id=${commemtList[i].children_commentList[j].user.id}`:`userInfo.html?id=${commemtList[i].children_commentList[j].user.id}`}">
                                           <span>${commemtList[i].children_commentList[j].user.nickname}：</span>
                                        </a>
                                        <div  class="userDesc">${fuwenben}</div>
                                     </div>
                                     <div  class="descInfo">
                                         <div class="commentHandle">
                                             <a href="javascript:;" class="handleZan ${commemtList[i].children_commentList[j].comment_praise==1?'active':''}" ${commemtList[i].children_commentList[j].praise_num==1?'active':''}" pid="${commemtList[i].children_commentList[j].pid}" id='${commemtList[i].children_commentList[j].id}'>
                                                 <i class="icon iconfont icon-zan"></i>
                                                 <span>${commemtList[i].children_commentList[j].praise_num}</span>
                                             </a>
                                             ·
                                             <a href="javascript:;"  class="handleReplay" aid=${names} pid="${commemtList[i].children_commentList[j].pid}" id='${commemtList[i].children_commentList[j].id}'>
                                                 回复
                                             </a>
                                             ·
                                             <span>${commemtList[i].children_commentList[j].create_date}</span>
                                         </div>
                                     </div>
                                 </div>`
                }
            }
        }
            let fuwenben=commemtList[i].content
            fuwenben = fuwenben.replace(/(\n)/g, "");    
            fuwenben = fuwenben.replace(/(\t)/g, "");    
            fuwenben = fuwenben.replace(/(\r)/g, ""); 
            var reg = new RegExp("<.+?>","g")   
            // fuwenben = fuwenben.replace(/<\/?[^>]*>/g, "");    
            // fuwenben = fuwenben.replace(/\s*/g, "");
        cstr=cstr+`<li>
                         <div class="userHead clearfix">
                             <div class="clearfix">
                                <img src="${commemtList[i].user.avatar}" alt="">
                                <div>
                                    <a target="_blank" href="${commemtList[i].user.type==0?`company.html?pkey=28&id=${commemtList[i].user.id}`:`userInfo.html?id=${commemtList[i].user.id}`}"><span style="color:#379be9;">${commemtList[i].user.nickname}</span></a><span>${commemtList[i].create_date}</span>
                                </div>
                             </div>
                         </div>
                         <div class="descInfo">
                             <div class="userDesc">
                                 ${fuwenben}
                             </div>
                             <div class="commentHandle">
                                 <a href="javascript:;" class="handleZan ${commemtList[i].comment_praise==1?'active':''}" pid="${commemtList[i].pid}" id='${commemtList[i].id}'>
                                     <i class="icon iconfont icon-zan"></i>
                                     <span>${commemtList[i].praise_num}</span>
                                 </a>
                                 ·
                                 <a href="javascript:;"  class="handleReplay" aid=${names} pid="${commemtList[i].pid}" id='${commemtList[i].id}'>
                                     回复
                                 </a>
                             </div>
                             <div class="moreAnswer ${commemtList[i].children_commentList.length>0?'':'hide'}">
                                <span>更多答复</span>
                             </div>
                             `+ccstr+showAll+`
                         </div>
                    </li>`
    }
    var comHtml=`<div class="commentList ${isDetail?'detailList':''}">
               <div class="commentNum clearfix ${isDetail?'hide':''}">
                    <span>${commemtList.length}条评论</span>
                    <a href="javascript:;"></a>
               </div>
               <div class="editPush ${isDetail?'hide':''}">
                    <div>
                        <div id=${'toolbar-container'+names} class="toolbar"></div>
                        <div id=${'text-container'+names} class="text"></div>
                    </div>
                    <a href="javascript:;">发布</a>
               </div>
               <ul>    
               `+cstr+`
               </ul>
               
           </div>`
    $(comHtml).appendTo(parent)
    // if(isDetail){
    //     commentEdit('toolbar-container'+names,'text-container'+names,names,isDetail)
    // }else{
        commentEdit('toolbar-container'+names,'text-container'+names,names)
    // }
    
    
    //点击回复按钮
    $(document).on('click','.handleReplay',function(){
        //避免点击事件重复绑定
        var timestamp = (new Date()).getTime()
        $('.handleReplay').off('click')
        $('.editPushItem').remove()
        var str=`<div class="editPush editPushItem">
                    <div>
                        <div id=${'toolbar-container'+timestamp} class="toolbar"></div>
                        <div id=${'text-container'+timestamp} class="text"></div>
                    </div>
                    <a>发布</a>
                </div>`
        $(str).appendTo($(this).parent())
        commentEdit('toolbar-container'+timestamp,'text-container'+timestamp,$(this).attr('aid'),$(this).attr('id'),isDetail)
    })

    //评论列表点击踩事件
    $('.requestList>ul ul li').off('click')
    $('.requestList ul').eq(0).on('click','.cancelZan',function(){
        var pid=$(this).attr('pid')
        var id=$(this).attr('id')
    })
    //评论列表点击赞事件
    $('.requestList>ul ul li').on('click','.handleZan',function(){
        var pid=$(this).attr('pid')
        var id=$(this).attr('id')
        commentPraise(id).then(res=>{
            $(this).find('span').html(res.data.praise_num)
            $(this).hasClass('active')?$(this).removeClass('active'):$(this).addClass('active')
            if(pid==0){
                commemtList.find(item=>item.id==id).praise_num=res.data.praise_num
                commemtList.find(item=>item.id==id).comment_praise=$(this).hasClass('active')?'1':'0'
            }else{
                var parentId=$(this).parents('.replayItem').siblings('.commentHandle').find('.handleZan').attr('id')
                commemtList.find(item=>item.id==parentId).children_commentList.find(item=>item.id==id).praise_num=res.data.praise_num
                commemtList.find(item=>item.id==parentId).children_commentList.find(item=>item.id==id).comment_praise=$(this).hasClass('active')?'1':'0'
            }
        })
    })
    // 查看全部 
    $('.requestList ul').eq(0).on('click','.showAll a',function(){
        $('.commentBox').css('display','flex')
        $(".allComments .topComment").html('')
        var id=$(this).attr('id')
        var commentItem=commemtList.find(item=>item.id==id)
        console.log(commentItem);
        //最上面的一级评论
        let fuwenben=commentItem.content
            // fuwenben = fuwenben.replace(/(\n)/g, "");    
            // fuwenben = fuwenben.replace(/(\t)/g, "");    
            // fuwenben = fuwenben.replace(/(\r)/g, "");    
            // fuwenben = fuwenben.replace(/<\/?[^>]*>/g, "");    
            // fuwenben = fuwenben.replace(/\s*/g, "");
        var str=`<li>
            <div class="userHead clearfix">
                <a target="_blank" href="${commentItem.user.type==0?`company.html?pkey=28&id=${commentItem.user.id}`:`userInfo.html?id=${commentItem.user.id}`}">
                   <img src="${commentItem.user.avatar}" alt="">
                   <span>${commentItem.user.nickname}</span>
                </a>
                <span>${commentItem.create_date}</span>
            </div>
            <div class="descInfo">
                <div class="userDesc">
                    ${fuwenben}
                </div>
                <div class="commentHandle">
                    <a href="javascript:;" class="handleZan ${commentItem.comment_praise==1?'active':''}" pid="${commentItem.pid}" id='${commentItem.id}'>
                        <i class="icon iconfont icon-zan"></i>
                        <span>${commentItem.praise_num}</span>
                    </a>
                    ·
                    <a href="javascript:;"  class="handleReplay" pid="${commentItem.pid}" id='${commentItem.id}'>
                        回复
                    </a>
                </div>
            </div>
       </li>`
        $(str).appendTo($(".allComments .topComment"))
        $(".allComments .topComment").on('click','.handleZan',function(){
            console.log($(this).attr('id'));
            commentPraise(id).then(res=>{
                $(this).find('span').html(res.data.praise_num)
                $(this).hasClass('active')?$(this).removeClass('active'):$(this).addClass('active')
            })
        })
        $(".allComments .topComment").on('click','.cancelZan',function(){
            // hasCai(commentItem,this)
            console.log($(this).attr('id'));
        })
        $('.repalyNum').html(commentItem.children_commentList.length+'条评论')
        commentChild(commentItem.children_commentList)
    })
    
}
//关闭查看所有评论弹出窗
$('.commentHead a').click(function(){
    $('.commentBox').css('display','none')
    $('#LAY_demo1').html('')
})
// function hasZan(data,ele){
//     if(data.praise_num==1){
//         data.likes--
//         data.praise_num=0
//         $(ele).removeClass('active')
//     }else{
//         if(data.praise_num==2){
//             $(ele).siblings('.cancelZan').children('span').html('踩')
//         }
//         data.likes++
//         data.praise_num=1
//         $(ele).addClass('active')
//     }
//     $(ele).children('span').html(data.likes)
// }

// function hasCai(data,ele){
//     if(data.praise_num==2){
//         data.praise_num=0
//         $(ele).children('span').html('踩')
//     }else{
//         if(data.praise_num==1){
//             data.likes--
//             $(ele).siblings('.handleZan').removeClass('active')
//         }
//         $(ele).children('span').html('取消踩')
//         data.praise_num=2
//     }
//     $(ele).siblings('.handleZan').children('span').html(data.likes)
// }
  //查看全部评论列表展示
  function commentChild(list){
    layui.use('flow', function(){
      var flow = layui.flow;
      flow.load({
        elem: '#LAY_demo1' //流加载容器
        ,scrollElem: '#LAY_demo1' //滚动条所在元素，一般不用填，此处只是演示需要。
        ,done: function(page, next){ //执行下一页的回调
          //模拟数据插入
        //   setTimeout(function(){
            var lis = [];
            console.log(list);
            for(let i = 0; i < list.length; i++){
                let fuwenben=list[i].content
                // fuwenben = fuwenben.replace(/(\n)/g, "");    
                // fuwenben = fuwenben.replace(/(\t)/g, "");    
                // fuwenben = fuwenben.replace(/(\r)/g, "");    
                // fuwenben = fuwenben.replace(/<\/?[^>]*>/g, "");    
                // fuwenben = fuwenben.replace(/\s*/g, "");
                lis.push(`<li>
                         <div class="userHead clearfix">
                             <a target="_blank" href="${list[i].user.type==0?`company.html?pkey=28&id=${list[i].user.id}`:`userInfo.html?id=${list[i].user.id}`}">
                                <img src="${list[i].user.avatar}" alt="">
                                <span>${list[i].user.nickname}</span>
                             </a>
                             <span>${list[i].create_date}</span>
                         </div>
                         <div class="descInfo">
                             <div class="userDesc">
                                ${fuwenben}
                             </div>
                             <div class="commentHandle">
                                 <a href="javascript:;" class="handleZan ${list[i].comment_praise==1?'active':''}" pid="${list[i].pid}" id='${list[i].id}'>
                                     <i class="icon iconfont icon-zan"></i>
                                     <span>${list[i].praise_num}</span>
                                 </a>
                                 ·
                                 <a href="javascript:;" class="handleReplay" pid="${list[i].pid}" id='${list[i].id}'>
                                     回复
                                 </a>
                             </div>
                         </div>
                    </li>`)
            }
            $("#LAY_demo1").on('click','.handleZan',function(){
                var id=$(this).attr('id')
                commentPraise(id).then(res=>{
                    console.log(res);
                    $(this).find('span').html(res.data.praise_num)
                    $(this).hasClass('active')?$(this).removeClass('active'):$(this).addClass('active')
                })
                // hasZan(list.find(item=>item.id==$(this).attr('id')),this)
            })
            $("#LAY_demo1").on('click','.cancelZan',function(){
                console.log(222222222222);
                // hasCai(list.find(item=>item.id==$(this).attr('id')),this)
            })
            next(lis.join(''), page < 1);
        // }, 500);
        }
      });
    });
  }

  function commentEdit(eleName,eles,aid,pid,isDetail){
    const E = window.wangEditor
    const editor = new E('#'+eleName,'#'+eles)
    const $text1 = $('#text1')
    editor.config.height='auto'
    editor.config.onchange = function (html) {
        // 第二步，监控变化，同步更新到 textarea
    }
    editor.config.compatibleMode = function () {
        // 返回 true 表示使用兼容模式；返回 false 使用标准模式
        return true
    }
    // 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
    editor.config.onchangeTimeout = 500 // 修改为 500 ms
    // 还可以修改历史记录的步数。默认 30 步
    editor.config.historyMaxSize = 50 // 修改为 50 步
    editor.config.linkImgCallback = function (src) {  //插入网络图片的回调
         console.log('图片 src ', src)
    }

    // 图片上传
    var newImg=''
    editor.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
    editor.config.customUploadImg = function (resultFiles, insertImgFn) {
        let formData= new FormData()
        let files = resultFiles[0]
        formData.append('file', files, files.name)
        $.ajax({
            method:"post",
            headers: {
                'token': tokens,
            },
            url:"https://api.guandaoxiufu.com/common/upload",
            data: formData,
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (res) {
                // if(res.code==1){
                //     layer.msg('上传成功')
                //     newImg=res.data.fullurl
                // }else{
                //     layer.msg(res.msg,{icon:5,shift:6})
                // }
                insertImgFn(res.data.fullurl)
            },
            error:function(err){
            }
        })
        // insertImgFn(imgUrl)
    }
    editor.config.showFullScreen = false //隐藏编辑器全屏按钮
    editor.config.uploadImgMaxLength = 1 //限制上传图片最多一张
    editor.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
    editor.config.showLinkImgHref = false// 隐藏网络图片的href
    editor.config.pasteFilterStyle = false
    editor.config.emotions = [
        {
            title: '表情', // tab 的标题
            type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
            content:emjoy
        },
    ]
    editor.config.menus = ['emoticon','image']
    editor.config.showLinkImg = false
    // emjoy
    editor.create()
    if(tokens){
        editor.enable()
    }else{
        editor.disable()
    }
    // 第一步，初始化 textarea 的值
    $text1.val(editor.txt.html())
    $('#'+eles).parent().parent().find('a').click(function(){
        if(tokens){
            if(editor.txt.html()==''){
                layer.msg('评论内容不能为空',{icon : 5,shift : 6});
            }else{
                var flag=false
                handleComments(aid,editor.txt.html(),pid,newImg).then(res=>{
                    if(res.code==1){
                        newImg=''
                        getCommentList(aid,1).then(res=>{
                            if(res.code==1){
                                if(!pid){
                                    commentInfo(res.data.commentList,$('#'+eles).parents('li').find('.commentContent'),aid)
                                }else{
                                    if(isDetail){
                                        commentInfo(res.data.commentList,$('#'+eles).parents('.requestList >ul'),aid,'detail')
                                    }else{
                                        commentInfo(res.data.commentList,$('#'+eles).parents('li').parents('li').find('.commentContent'),aid)
                                    }
                                }
                            }else{
                                layer.msg(res.msg,{icon : 5,shift : 6});
                            }
                        })
                    }else{
                        layer.msg(res.msg,{icon : 5,shift : 6});
                    }
                })
            }
        }else{
            window.location.href='login.html?history=1'
        }
    })
  }

