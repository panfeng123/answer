proSmallApi(JSON.parse(localStorage.getItem('userInfo')).id).then(res=>{
    console.log(res.data);
    $.each(res.data, function (index, item) {
        $('select[name=category]').append(new Option(item.name, item.id));// 下拉菜单里添加元素
        layui.form.render("select");
    });
}).catch(err=>{})
$('.handleCancel').click(function(){
  $('.release').css('display','none')
      editrs.txt.clear()
      $('#formId')[0].reset();
})
const Ed = window.wangEditor
const editrs = new Ed('#edit1')
editrs.config.onchange = function (html) {
    // 第二步，监控变化，同步更新到 textarea
    var re = /<img[^>]+>/g;
}
editrs.config.compatibleMode = function () {
    // 返回 true 表示使用兼容模式；返回 false 使用标准模式
    return true
}
// 当我们使用兼容模式的时候，可以自定义记录的频率，默认 200 ms
editrs.config.onchangeTimeout = 500 // 修改为 500 ms
// 还可以修改历史记录的步数。默认 30 步
editrs.config.historyMaxSize = 50 // 修改为 50 步
editrs.config.linkImgCallback = function (src) {  //插入网络图片的回调
}

// 图片上传
editrs.config.uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'webp','jfif']
editrs.config.customUploadImg = function (resultFiles, insertImgFn) {
    let formData= new FormData()
    let files = resultFiles[0]
    formData.append('file', files, files.name)
    $.ajax({
        method:"post",
        headers: {
            'token': tokens,
        },
        url:"https://api.guandaoxiufu.com/common/upload",
        data: formData,
        dataType: "json",
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            insertImgFn(res.data.fullurl)
        },
        error:function(err){
        }
    })
    // insertImgFn(imgUrl)
}
editrs.config.showFullScreen = false //隐藏编辑器全屏按钮
editrs.config.uploadImgMaxLength = 1 //限制上传图片最多一张
editrs.config.showLinkImgAlt = false// 隐藏网络图片的alt配置
editrs.config.showLinkImgHref = false// 隐藏网络图片的href
editrs.config.pasteFilterStyle = false
editrs.config.emotions = [
    {
        title: 'QQ', // tab 的标题
        type: 'image', // 'emoji' 或 'image' ，即 emoji 形式或者图片形式
        content:emjoy
    },
]
editrs.config.menus = [
    'head',
    'bold',
    'fontSize',
    'fontName',
    'italic',
    'underline',
    'strikeThrough',
    'indent',
    'foreColor',
    'backColor',
    'list',
    'justify',
    'quote',
    'emoticon',
    'image',
    'splitLine',
    'undo',
]
// emjoy
editrs.create()
// 第一步，初始化 textarea 的值

var image=''
layui.use(['form', 'layedit', 'laydate','upload'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
  var $ = layui.jquery
    ,upload = layui.upload;
  upload.render({
         elem: '#form1'
         ,url: 'https://api.guandaoxiufu.com/common/upload' //改成您自己的上传接口
       ,auto:false
         ,choose: function(obj){
       obj.preview(function(index, file, result){
       var formData= new FormData()
      formData.append('file', file, file.name)
       $.ajax({
         type: "post",
         url: "https://api.guandaoxiufu.com/common/upload",
         data:formData,
         contentType:false,
         dataType: "json",
         processData: false,
         success: function (res) {
            layui.$('#uploadDemoView').removeClass('layui-hide').find('img').attr('src', res.data.fullurl);
            $('#uploadDemoView').siblings().hide()
            image=res.data.fullurl
         }
      });	
     });
     
         }
    });  
  //自定义验证规则
  form.verify({
    title: function(value){
          if(value.length < 1){
            return '标题不能为空';
          }
        }
    ,content: function(value){
      layedit.sync(editIndex);
    }
    ,description:function(value){
        if(!value){
          return '简介不能为空';
        }
      }
  });
  
  //监听提交
  form.on('submit(form1)', function(data){
    if(!image){
        layer.msg('请上传图片',{
            icon : 5,
            shift : 6, 
        });
        return false
    }else{
        data.field.image=image
    }
    if(!editrs.txt.html()){
        layer.msg('请输入正文',{
            icon : 5,
            shift : 6, 
        });
        return false
    }else{
        data.field.content=editrs.txt.html()
        console.log(data.field);
    }
   
    articlePush(data.field).then(res=>{
        if(res.code==1){
            image=''
            layer.msg(res.msg);
            layui.$('#uploadDemoView').addClass('layui-hide').find('img').attr('src', ' ');
            $('#uploadDemoView').siblings().show()
            $('.release').css('display','none')
             editrs.txt.clear()
                $('#formId')[0].reset();
            var currentId=''
            if(getUrlParam('pkey')!==null){
                $('.request_menu ul li[id='+pkey+']').addClass("active")
                currentId=pkey
            }else{
                $('.request_menu ul li').eq(0).addClass("active")
                currentId=$('.request_menu ul li').eq(0).attr('id')
            }
            //根据menu的id获取列表数据
            isSearch=getUrlParam('isSearch'),keys=getParamFromUrl('keys'),channel=getUrlParam('channel')
            if(isSearch){
                currentId=channel
            }
            getTable(currentId,0,10).then(res=>{
                menuClick($('.request_menu ul li').eq(0).attr('id'),res)
            }).catch(err=>{

            })
        }else{
            layer.msg(res.msg,{icon : 5,shift : 6});
        }
    })
    
    return false;
  });
});
window.wordLeg = function (obj) {
    var currleg = $(obj).val().length;
    var length = $(obj).attr('maxlength');
    if (currleg > length) {
        layer.msg('字数请在' + length + '字以内');
      }else{
         $('.text_count').text(currleg);
     }
}