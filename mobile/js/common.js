//时间转换
function formatDate(dates,type) {
	var date = new Date(dates*1000);
	var YY = date.getFullYear() ;
	var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
	var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	return YY + '年'
	// if(type==1){
	// 	return YY + MM + DD 
	// }else if(type==2){
	// 	return YY + MM + DD +" "+hh + mm + ss;
	// }
  }

//超过一万保留两位小数  
function unitZan(num){
	var n
	if(num>10000){
	  n = Math.floor((num /10000) * 100) / 100;
	  return n + "万";
	}else if(num==0){
	  return ''
	}else{
		return num
	}
}


//等级图标
function levelImg(level){
	var levelHtml=''
	if(level){
		var sunNum=parseInt(level/16)
		var moonNum=parseInt(level/4%4)
		var starNum=parseInt(level%4)
		for(let i=0;i<sunNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_sun.png" alt="">'
		}
		for(let i=0;i<moonNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_moon.png" alt="">'
		}
		for(let i=0;i<starNum;i++){
			levelHtml=levelHtml+'<img src="./images/level_star.png" alt="">'
		}
	}
	return levelHtml
}

  //获取参数
  function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

//获取中文参数
function getParamFromUrl(name){
    var r = getParamString(name)
    return r
  }
    //获取参数
function getParamString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return decodeURI(r[2]); return null; //返回参数值
}

 //跳转详情
 function toDetail(){
	$('section').off('click','.jumpTodetail')
	$('section').on('click','.jumpTodetail',function(){
		var ids=$(this).attr('id')
		var pkey=$(this).attr('pkey')
		console.log(ids);
		console.log(pkey);
		var url=$(this).attr('urls')
		getDetail(ids).then(res=>{
			if(res.code==1){
				window.location.href=`${url}?pkey=${pkey}&id=${ids}`
			}else{
				layer.open({
				  type: 1
				  ,title: false //不显示标题栏
				  ,closeBtn: false
				  ,area: '300px;'
				  ,shade: 0.8
				  ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
				  ,btn: ['确定', '取消']
				  ,btnAlign: 'c'
				  ,moveType: 1 //拖拽模式，0或者1
				  ,content: '<div style="padding: 40px 20px;text-align: center; line-height: 22px; background-color: #fff; color: #444; font-weight: 300;">'+res.msg+'</div>'
				  ,success: function(layero){
					var btn = layero.find('.layui-layer-btn');
					btn.find('a').eq(0).click(function(){
						deductIntegral(ids).then(res=>{
							if(res.code==1){
								getDetail(ids).then(res=>{
									if(res.code==1){
										window.location.href=`${url}?pkey=${pkey}&id=${ids}`
									}
								})
							}else{
								layer.msg(res.msg)
							}
						})
					})
					
				  }
				});
			}
		})
	})
}