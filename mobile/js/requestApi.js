var tokens=sessionStorage.getItem("token")?JSON.parse(sessionStorage.getItem("token")):''
//首页
function headerInfo(){
    return new Promise((res,rej)=>{
      $.ajax({
        type: "post",
        headers:{"Content-Type":"application/x-www-form-urlencoded"},
        url: "https://api.guandaoxiufu.com/cms/index",
        dataType: "json",
        success: function (datas) {
            sessionStorage.setItem("menuView", JSON.stringify(datas));
            res(datas)
        },
        error:function(err){
          rej(err)
        }
    });
    })
  }
    //有问必答
    function iquestTableList(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_ywbd",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //配套商家
      function ishopTableList(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_ptsj",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //热心行家
      function ihotUserList(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_hot_user",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //项目找队伍
      function ifindteam(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_project",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //找产品&服务
      function ifinduser(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_zxmfw",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
    //   招标信息
    function itenderInfo(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_zbxx",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //   工程资料
    function ishowProjectInfo(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_zl",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //   工程案例
    function ishowProjectCase(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_al",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
    //   培训展会
    function ishownewsExchange(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_pxzh",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
      //   业内新闻
    function ishownewsInfo(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_ynxw",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
       //   领军人物
    function ishowleaderInfo(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_ljrw",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
        //   前沿技术
    function ishowadvancedScience(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_qyjs",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
         //   人才流动
    function ishowtalentFlow(){
        return new Promise((res,rej)=>{
          $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded"},
            url: "https://api.guandaoxiufu.com/cms/index/index_talent_flow",
            data:{},
            dataType: "json",
            success: function (datas) {
                res(datas)
            },
            error:function(err){
              rej(err)
            }
        });
        })
      }
//搜索接口
function aticleSearch(search,channel_id,page,pagenums){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/search/index",
            data:{'search':search,'channel_id':channel_id,'page':page,'pagenums':pagenums},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}
        });
    })
}
// 获取评论列表
function getCommentList(aid,page){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/json","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/comment/index",
            data:JSON.stringify({"aid":aid,"page":page}),
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}
        });
    })
}


// 详情接口
function getDetail(diyname){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/detail",
            data:{"diyname":diyname},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}
        });
    })
}

// 申请查看详情积分扣除接口
function deductIntegral(id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/apply",
            data:{"id":id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}
        });
    })
}

// 发布评论接口
function handleComments(aid,content,pid,images){
    let parmas={"aid":aid,'content':encodeURI(content)}
    if(pid){
        parmas.pid=pid
    }
    if(images){
        parmas.images=images
    }
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/json","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/comment/post",
            data:JSON.stringify(parmas),
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//文章点赞接口
function zanApi(aid){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/praise",
            data:{"id":aid},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}



//被邀请绑定接口
function inviteBind(){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/user/bind_list",
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}




//侧边栏接口
function sideData(channel,type,user_id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/right_list",
            data:{'channel':channel,'type':type,"user_id":user_id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//用户列表
function userInfoList(keywords,type){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/user/account",
            data:{'keywords':keywords,'type':type},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}


//邀请回答
function inviteUserApi(archive_id,invited_id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/invite_posts",
            data:{'archive_id':archive_id,'invited_id':invited_id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}


//发布文章
function articlePush(datas){
    datas.content=encodeURI(datas.content)
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/post",
            data:datas,
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//个人中心和公司中心
function presonalApi(id,category,page,cate){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/user/index",
            data:{'id':id,'category':category,'page':page,'cate':cate},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//收藏接口
function collectApi(id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/collect",
            data:{'id':id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//收藏列表接口
function collectList(page,pagenums){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/collect_archives",
            data:{'page':page,'pagenums':pagenums},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//合作投稿接口
function teamworkApi(){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/index/cooperation",
            data:{},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}



//公司发起绑定
function bingUserApi(username,content){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/user/bind",
            data:{"username":username,'content':content},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//用户确认绑定
function userConfirmApi(launch_id,status){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/user/is_bind",
            data:{"launch_id":launch_id,'status':status},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//用户确认绑定
function bindCalcelApi(){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/user/relieve_bind",
            data:{},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}


//产品分类下的小分类列表
function proSmallApi(user_id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/companycatelist",
            data:{'user_id':user_id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                // layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}


//关注功能
function focuseAuthor(author_id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/follows/add",
            data:{"author_id":author_id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//发展站内信
function sendEmail(datas){
    datas.content=encodeURI(datas.content)
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/msgs/send",
            data:datas,
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//新增产品小分类
function proSmallTypeApi(datas){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/companycatepost",
            data:datas,
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//删除产品小分类
function delProSmallType(id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/companycatedel",
            data:{"id":id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//评论点赞
function commentPraise(id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/comment/praise",
            data:{"id":id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//评论点赞
function headerImg(){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/index/commons",
            data:{},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}

//申请查看联系人信息接口
function callIview(id){
    return new Promise((res,rej)=>{
        $.ajax({
            type: "post",
            headers:{"Content-Type":"application/x-www-form-urlencoded","token":tokens},
            url: "https://api.guandaoxiufu.com/cms/archives/contact_apply",
            data:{'id':id},
            dataType: "json",
            success: function (data) {
                res(data)
            },
            error:function(err){
                var errs=JSON.parse(err.responseText)
                layer.msg(errs.msg,{icon : 5,shift : 6});
                rej(errs)
			}

        });
    })
}