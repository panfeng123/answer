var tokens=localStorage.getItem("token")?JSON.parse(localStorage.getItem("token")):''
var userinfo=JSON.parse(localStorage.getItem("userInfo"));
if(getUrlParam('pkey')!==null){
    $('.request_menu ul li[id='+getUrlParam('pkey')+']').addClass("active").siblings().removeClass('active')
  }else{
    $('.request_menu ul li').eq(0).addClass("active").siblings().removeClass('active')
  }

// 判断是个人还是公司
  var mianUserUrl=''
if(userinfo){
  if(userinfo.type===0){
    mianUserUrl='company.html' 
  }else{
    mianUserUrl='userInfo.html'
  }
}
headerImg().then(res=>{
  $('header .container>div>img').attr('src',res.data.mobile_sitelogo)
  $('.headContent>p').attr('src',res.data.sitename)
  $('.qrCode img').eq(0).attr('src',res.data.qrcode[0])
  $('.qrCode img').eq(1).attr('src',res.data.qrcode[1])
  if(res.code==1){
    if(window.location.href.indexOf('index.html')!==-1||window.location.href.indexOf('company.html')!==-1||window.location.href.indexOf('userInfo.html')!==-1){
      var keywords=$("meta[name=keywords]")[0];
      keywords.content=res.data.keywords
      var description=$("meta[name=description]")[0];
      description.content=res.data.description
      $('title').html(res.data.title+'-管道修复网')
    }
  }
})
// 登录这显示头像
if(!tokens){
    $('.search_btn').removeClass('haslogin')
    var str=`<a href="login.html">登录</a>
    <i></i>
    <a href="register.html">注册</a>`
}else{  
    console.log(1);
    $('.search_btn').addClass('haslogin')
    // userInfo.html?id=${userinfo.id}
    var str=`<div class="userInfo">
                <img class="userImages" src="${userinfo.avatar?userinfo.avatar:''}" alt="">
                <span class="userNickName" style="font-size:14px;">${userinfo.nickname?userinfo.nickname:''}</span>
                <div class="userHandle">
                     <div>
                         <span></span>
                         <div>
                             <p><a class="jumpToUser" href="${mianUserUrl}?id=${userinfo.id}">我的主页</a></p>
                             
                             <p><a class="userExit" href="javascript:;">退出</a></p>
                         </div>
                     </div>
                 </div>
            </div>`
}
{/* <p><a class="editInfo" href="javascript:;">修改个人资料</a></p> */}
var menu_open=false
$('.mobile_search').click(function(){
    menu_open=!menu_open
    if(menu_open){
        $('.right_menu').addClass('right_menu_open')
    }else{
        $('.right_menu').removeClass('right_menu_open')
    }
})
$('.right_menu> ul img').click(function(){
    menu_open=!menu_open
    if(menu_open){
        $('.right_menu').addClass('right_menu_open')
    }else{
        $('.right_menu').removeClass('right_menu_open')
    }
})
$('.toMyOwner').click(function(){
  if(userinfo){
      window.location.href=`${mianUserUrl}?id=${userinfo.id}`
  }else{
    layer.msg('请先登录',{icon:5,shift:6})
  }
})
$(str).appendTo($('.search_btn'))
$(document).click(function(e){
    var e = e || window.event; //浏览器兼容性
    var elem = e.target || e.srcElement;
    if(elem.className=='userImages'||elem.className=='userNickName'){
      $('.userHandle').css('display')=='none'?$('.userHandle').css('display','block'):$('.userHandle').css('display','none')
    }else if(elem.className=='userExit'){
          localStorage.clear()
          window.location.href='login.html'
    }else{
      $('.userHandle').css('display','none')
    }
    // if(elem.className=='userImages')
})


    
layui.use(['form'], function(){
    var form = layui.form
    ,layer = layui.layer
    form.on('radio(ChoiceRadio)', function(data){
      console.log(data); //得到 radio 原始 DOM 对象
    });    
  });  
  var bigSearchType=''
  var demo2 = xmSelect.render({
    el: '#demo2', 
    radio: true,
    clickClose: true,
    theme: {
      color: '#ff9900',
      },
      repeat: true,
            direction: 'down',
    data: [
      {name: '有问必答', value: 26, href:'answerMust.html' ,selected: true,},
      {name: '项目牵线', value: 27, href:'project.html'},
      {name: '配套商家', value: 28, href:'merchants.html'},
      {name: '资料案例', value: 29, href:'material.html'},
      {name: '人物技术', value: 30, href:'technology.html'},
      {name: '新闻交流', value: 31, href:'newExchange.html'},
    ],
    on: function(data){
      console.log(data);
      bigSearchType=data
    },
  })
  $('.searchBox .icon-search').click(function(){
      if($(this).prev().val()===''){
        if(bigSearchType){
          window.location.href=bigSearchType.change[0].href
        }else{
          window.location.href='answerMust.html'
        }
      }else{
        if(bigSearchType){
          window.location.href=bigSearchType.change[0].href+'?isSearch=search&keys='+$(this).prev().val()+'&channel='+bigSearchType.change[0].value
        }else{
          window.location.href='answerMust.html?isSearch=search&keys='+$(this).prev().val()+'&channel=26'
        }
      }
  })
  
  $('.pageCollect').click(function(){
    _addFavorite()
  })
  function _addFavorite() {
    var url = window.location;
    var title = document.title;
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("360se") > -1) {
        alert("由于360浏览器功能限制，请按 Ctrl+D 手动收藏！");
    }
    else if (ua.indexOf("msie 8") > -1) {
        window.external.AddToFavoritesBar(url, title); //IE8
    }
    else if (document.all) {//IE类浏览器
      try{
       window.external.addFavorite(url, title);
      }catch(e){
       alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
      }
    }
    else if (window.sidebar) {//firfox等浏览器；
        window.sidebar.addPanel(title, url, "");
    }
    else {
        alert('您的浏览器不支持,请按 Ctrl+D 手动收藏!');
    }
  }  
  var hrefUrl=window.location.href.replace(/m.guandaoxiufu.com/, "www.guandaoxiufu.com")
    if($(window).width()>640){
      window.location.href=hrefUrl
    }
