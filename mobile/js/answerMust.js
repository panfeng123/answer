//有问必答列表
function tablelist(questList){
  var qstr=''
  $('.requestList>ul').html('')
  for(let i=0;i<questList.length;i++){
      questList[i].showComments=false
      var isZan=unitZan(questList[i].likes)
      if(questList[i].comment.user_info){
          var accountLevel=levelImg(questList[i].comment.user_info.level)
      }else{
          accountLevel=''
      }
      var userLevel=levelImg(questList[i].user_info.level)
      qstr=qstr+`<li>
      <div class="jumpTodetail" ids=${questList[i].id} pkey=${questList[i].channel_id}>
        <div class="titleBox clearfix">
            <p class="listTitle singleHide">【${questList[i].channel.name}】${questList[i].title}</p>
            <a class="userInfo" href="${questList[i].user_info.id!=1?(questList[i].user_info.type===0?`company.html?id=${questList[i].user_info.id}`:`userInfo.html?id=${questList[i].user_info.id}`):'javascript:;'}">
                <span class="userName">${questList[i].user_info.nickname}</span>
                <span class="level">
                  `+userLevel+`
                </span>
            </a>
        </div> 
        <div class="lisTxt clearfix">
            <img src="${questList[i].comment.images?questList[i].comment.images:''}" class="${questList[i].comment.images?'':'hide'}">
            <div class="threeHide ${questList[i].comment.images?'newDesc':''}">${questList[i].comment.content?questList[i].comment.content:''}</div>
        </div>
      </div>
      <div class="timeAndZan clearfix">
            <div class="zanLeft">
                <a href="javascript:;" class="favor  ${questList[i].is_praise==1?'upFavor':''}"  index=${i} itemId=${questList[i].id}>
                    <i class="icon iconfont icon-up"></i>
                    <span class="zanValue">${questList[i].is_praise==1?'已赞成':'赞成'}</span> <span class="zanNum">${isZan}</span>
                </a>
                <a href="javascript:;" class="cancelfavor" index=${i}  itemId=${questList[i].id}>
                    <i class="icon iconfont icon-down"></i>
                </a>
                <a href="javascript:;" class="commentIcon ${questList[i].is_praise==2?'downFavor':''}" index=${i} ids=${questList[i].id}>
                    <i class="icon iconfont icon-message showComment"></i>
                    <span class="showComment">${questList[i].comments} 评论</span>
                </a>   
            </div>
            <a class="zanRight" href="${questList[i].comment.user_info.id!=1?(questList[i].comment.user_info.type==0?`company.html?id=${questList[i].comment.user_info.id}`:`userInfo.html?id=${questList[i].comment.user_info.id}`):'javascript:;'}">
                <span>${questList[i].comment.user_info?(questList[i].comment.user_info.nickname?questList[i].comment.user_info.nickname+'：':''):''}</span>
                <span>
                  `+accountLevel+`
                </span>
            </a>
      </div>
      <div class="commentContent"></div>
      <div class="pages"></div>
   </li>`
  }
  $(qstr).appendTo($('.requestList>ul'))
  $('.requestList>ul').off('click')
            $('.requestList>ul').on('click','.jumpTodetail',function(){
                var ids=$(this).attr('ids')
                var pkey=$(this).attr('pkey')
                getDetail($(this).attr('ids')).then(res=>{
                    if(res.code==1){
                        window.location.href=`answerBody.html?pkey=${pkey}&id=${ids}`
                    }else{
                        layer.open({
                          type: 1
                          ,title: false //不显示标题栏
                          ,closeBtn: false
                          ,area: '300px;'
                          ,shade: 0.8
                          ,id: 'LAY_layuipro' //设定一个id，防止重复弹出
                          ,btn: ['确定', '取消']
                          ,btnAlign: 'c'
                          ,moveType: 1 //拖拽模式，0或者1
                          ,content: '<div style="padding: 40px 20px;text-align: center; line-height: 22px; background-color: #fff; color: #444; font-weight: 300;">'+res.msg+'</div>'
                          ,success: function(layero){
                            var btn = layero.find('.layui-layer-btn');
                            btn.find('a').eq(0).click(function(){
                                deductIntegral(ids).then(res=>{
                                    if(res.code==1){
                                        getDetail(ids).then(res=>{
                                            if(res.code==1){
                                                window.location.href=`answerBody.html?pkey=${pkey}&id=${ids}`
                                            }
                                        })
                                    }else{
                                        layer.msg(res.msg,{icon : 5,shift : 6});
                                    }
                                })
                            })
                            
                          }
                        });
                    }
                })
            })
            $('.requestList>ul').on('click','.favor',function(){
                var currnetItem=questList.find(item=>item.id==$(this).attr('itemId'))
                var index=$(this).attr('index')
                if(tokens){
                    if(currnetItem.is_praise!==1){
                        questList[index].is_praise=1
                        zanApi($(this).attr('itemId')).then(res=>{
                            if(res.code==1){
                                var zans=unitZan(res.data.likes)
                                $(this).children('.zanNum').html(zans)
                                $(this).children('.zanValue').html('已赞成')
                                $(this).addClass('upFavor').siblings().removeClass('downFavor')
                            }else{
                                layer.msg(res.msg,{icon : 5,shift : 6});
                            }
                        }).catch(err=>{
                        
                        })
                    }else{
                        questList[index].is_praise=0
                        zanApi($(this).attr('itemId')).then(res=>{
                            if(res.code==1){
                                var zans=unitZan(res.data.likes)
                                $(this).children('.zanNum').html(zans)
                                $(this).children('.zanValue').html('赞成')
                                $(this).removeClass('upFavor')
                            }else{
                                layer.msg(res.msg,{icon : 5,shift : 6});
                            }
                        }).catch(err=>{
                        
                        })
                    }
                }else{
                    zanApi($(this).attr('itemId')).then(res=>{
                        if(res.code==1){
                            var zans=unitZan(res.data.likes)
                            $(this).children('.zanNum').html(zans)
                            $(this).children('.zanValue').html('赞成')
                        }else{
                            layer.msg(res.msg,{icon : 5,shift : 6});
                        }
                    }).catch(err=>{
                    
                    })
                }
            })
            $('.requestList>ul').on('click','.cancelfavor',function(){
                var currnetItem=questList.find(item=>item.id==$(this).attr('itemId'))
                var index=$(this).attr('index')
                if(currnetItem.is_praise==1){
                    questList[index].is_praise=0
                    zanApi($(this).attr('itemId')).then(res=>{
                        if(res.code==1){
                            var zans=unitZan(res.data.likes)
                            $(this).siblings('.favor').removeClass('upFavor')
                            $(this).siblings('.favor').children('.zanNum').html(zans)
                            $(this).siblings('.favor').children('.zanValue').html('赞成')
                        }else{
                            layer.msg(res.msg,{icon : 5,shift : 6});
                        }
                    }).catch(err=>{
                    
                    })
                }
                // if(currnetItem.is_praise!==2){
                //     if(questList[index].is_praise==1){
                //         questList[index].likes--
                //         var zans=unitZan(questList[index].likes)
                //         $(this).siblings('.favor').children('.zanNum').html(zans)
                //         $(this).siblings('.favor').children('.zanValue').html('赞成')
                //     }
                //     $(this).addClass('downFavor').siblings().removeClass('upFavor')
                //     questList[index].is_praise=2
                // }else{
                //     $(this).removeClass('downFavor')
                //     questList[index].is_praise=0
                // }
            })
            $('.requestList>ul').on('click','.commentIcon',function(){
                var ids=$(this).attr('ids')
                var index=$(this).attr('index')
                var showcomments=questList[index].showComments
                if(!showcomments){
                    questList[index].showComments=!questList[index].showComments
                    getCommentList(ids,1).then(res=>{
                        commentInfo(res.data.commentList,$(this).parent().parent().siblings('.commentContent'),ids)
                        var page=`<div id=${'pages'+ids} class="pageUi"></div>`
                        $(page).appendTo($(this).parent().parent().siblings('.pages'))
                        commentPage(ids,res.data.total_comment,$(this).parent().parent().siblings('.commentContent'))
                    })
                }else{
                    questList[index].showComments=!questList[index].showComments
                    $(this).parent().parent().siblings().find('.commentList').remove()
                    $(this).parent().parent().siblings().find('#pages'+ids).remove()
                }
            })
}



//评论分页
function commentPage(names,pageCount,ele){
    layui.use(['laypage', 'layer'], function(){
        var laypage = layui.laypage,layer = layui.layer;
        laypage.render({
            elem: 'pages'+names
            ,groups:4
            ,count: pageCount
            ,first: '首页'
            ,prev: '上一页'
            ,next: '下一页'
            ,last: '尾页'
            ,jump: function(obj, first){
                if(!first){
                    $(this).parent().parent().siblings().find('.commentList').remove()
                    getCommentList(names,obj.curr).then(res=>{
                        commentInfo(res.data.commentList,ele,names)
                    })
                }
            }
        });
    })
}

//小类型搜索
var smallType={name: '全部', value: 26, pkey:26,selected: true,}
var searchSelect = xmSelect.render({
  el: '#searchSelect', 
  radio: true,
  clickClose: true,
  theme: {
    color: '#ff9900',
    },
    repeat: true,
            direction: 'down',
  data: [
    {name: '全部', value: 26, pkey:26,selected: true,},
    {name: '施工工艺', value: 32, pkey:26},
    {name: '设备材料', value: 33, pkey:26,},
    {name: '验收结算', value: 34, pkey:26,},
    {name: '其他', value: 35, pkey:26,},
  ],
  on: function(data){
    smallType=data.change[0]
  },
})


$('.hasSearch').click(function(){
    isSearch='isSearch',keys=$('.searchValue').val(),channel=smallType.value
    $('.request_menu ul li#'+smallType.value).addClass('active').siblings().removeClass('active')
    getTable(channel,0,10).then(res=>{
        menuClick($(this).attr("id"),res)
    }).catch(err=>{

    })
})